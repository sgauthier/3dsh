#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <3dmr/skybox.h>
#include <3dmr/img/png.h>
#include <3dmr/render/texture.h>
#include "builtins.h"
#include "shell.h"
#include "texture.h"
#include "variable.h"
#include "render.h"

static void tex_destroy_(struct DemoShell* shell, void* d) {
    if (!d) return;
    if (!pthread_mutex_lock(&shell->swapMutex)) {
        GLuint* tex = &((struct TextureData*)d)->tex;
        viewer_make_current(shell->viewer);
        if (*tex) glDeleteTextures(1, tex);
        viewer_make_current(NULL);
        pthread_mutex_unlock(&shell->swapMutex);
    }
    free(((struct TextureData*)d)->description);
    free(d);
}

static struct ShellRefCount* shell_new_texture(struct DemoShell* shell, const unsigned char* data, unsigned int width, unsigned int height, unsigned int channels, unsigned int ralign) {
    struct ShellRefCount* r;
    struct TextureData* td;
    unsigned int rsize = width * channels, dsize;
    rsize += (ralign - (rsize % ralign)) % ralign;
    dsize = rsize * height;

    if (!(r = shell_new_refcount(shell, tex_destroy_, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return NULL;
    }
    if (!(td = malloc(sizeof(*td) + dsize))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        return NULL;
    }
    td->description = NULL;
    td->w = width;
    td->h = height;
    td->c = channels;
    td->a = ralign;
    td->interp = GL_NEAREST;
    if (pthread_mutex_lock(&shell->swapMutex)) {
        fprintf(stderr, "Error: failed to lock mutex\n");
        shell_decref(r);
        free(td);
        return NULL;
    }
    viewer_make_current(shell->viewer);
    td->tex = texture_load_from_uchar_buffer(data, width, height, channels, ralign);
    glBindTexture(GL_TEXTURE_2D, td->tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, td->interp);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, td->interp);
    viewer_make_current(NULL);
    pthread_mutex_unlock(&shell->swapMutex);
    memcpy(td + 1, data, dsize);
    r->data = td;
    return r;
}

int tex_description_set(const struct Variable* src, void* dest) {
    struct TextureData* td = ((struct ShellRefCount*)dest)->data;
    return string_ptr_set(src, &td->description);
}

int tex_ptr_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dpointer.sdata)) return 0;
    variable_make_ptr(src->data.dpointer.info, src->data.dpointer.gdata, src->data.dpointer.sdata, dest);
    return 1;
}

void tex_ptr_free(struct Variable* v) {
    shell_decref(v->data.dpointer.sdata);
}

struct PointerInfo texDescription = {string_ptr_get, tex_description_set, tex_ptr_copy, tex_ptr_free};

static int tex_description(const void* src, struct Variable* dest) {
    struct TextureData* td = ((const struct ShellRefCount*)src)->data;
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&texDescription, &td->description, (void*)src, dest);
    return 1;
}

#define TEX_MEMBER_INT(name) \
static int tex_##name(const void* src, struct Variable* dest) { \
    struct TextureData* td = ((const struct ShellRefCount*)src)->data; \
    variable_make_int(td->name, dest); \
    return 1; \
}

TEX_MEMBER_INT(tex)
TEX_MEMBER_INT(w)
TEX_MEMBER_INT(h)
TEX_MEMBER_INT(c)
TEX_MEMBER_INT(a)

static int tex_interp_get(const void* src, struct Variable* dest) {
    const struct TextureData* td = ((const struct ShellRefCount*)src)->data;
    switch (td->interp) {
        case GL_NEAREST: return variable_make_string("nearest", 7, dest);
        case GL_LINEAR:  return variable_make_string("linear", 6, dest);
        default:       return variable_make_string("invalid", 7, dest);
    }
    return 0;
}

static int tex_interp_set(const struct Variable* src, void* dest) {
    char* s;
    const struct ShellRefCount* r = dest;
    struct TextureData* td = r->data;
    int ret = 1;
    if ((s = variable_to_string(src))) {
        if (!strcmp(s, "nearest")) {
            td->interp = GL_NEAREST;
        } else if (!strcmp(s, "linear")) {
            td->interp = GL_LINEAR;
        } else {
            ret = 0;
        }
        free(s);
    } else {
        ret = 0;
    }
    if (ret) {
        if (!pthread_mutex_lock(&r->shell->swapMutex)) {
            viewer_make_current(r->shell->viewer);
            glBindTexture(GL_TEXTURE_2D, td->tex);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, td->interp);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, td->interp);
            viewer_make_current(NULL);
            pthread_mutex_unlock(&r->shell->swapMutex);
        }
    }
    return ret;
}

struct PointerInfo texInterp = {tex_interp_get, tex_interp_set, tex_ptr_copy, tex_ptr_free};

static int tex_interp(const void* src, struct Variable* dest) {
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&texInterp, (void*)src, (void*)src, dest);
    return 1;
}

static const struct FieldInfo structTextureFields[] = {
    {"description", tex_description},
    {"id", tex_tex},
    {"width", tex_w},
    {"height", tex_h},
    {"channels", tex_c},
    {"rowAlign", tex_a},
    {"interpolation", tex_interp},
    {0}
};

static const struct StructInfo structTexture;

static int tex_copy_(const struct Variable* src, struct Variable* dest) {
    void* data = src->data.dstruct.data;
    if (!shell_incref(data)) return 0;
    variable_make_struct(&structTexture, data, dest);
    return 1;
}

static void tex_free_(void* data) {
    shell_decref(data);
}

static const struct StructInfo structTexture = {"Texture", structTextureFields, tex_copy_, tex_free_};

static unsigned int parse_color(const struct Variable* arg, Vec4 color) {
    if (variable_to_vec4(arg, color)) return 4;
    if (variable_to_vec3(arg, color)) return 3;
    if (variable_to_vec2(arg, color)) return 2;
    if (variable_to_float(arg, color)) return 1;
    return 0;
}

static int texture(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    union TexParam {
        char* path;
        Vec4 color;
    } param;
    unsigned char* d;
    char* description = NULL;
    struct ShellRefCount* r;
    struct DemoShell* shell = data;
    unsigned int i, width, height, channels;

    if (nArgs != 1 && nArgs != 3 && nArgs != 4) {
        fprintf(stderr, "Error: texture: expected 1 path/color, [width, height[, channels]] optional for color\n");
        return 0;
    }
    if (nArgs == 1 && (param.path = variable_to_string(args))) {
        if (!png_read(param.path, 4, &width, &height, &channels, 0, 1, &d)) {
            free(param.path);
            return 0;
        }
        description = param.path;
    } else if ((channels = parse_color(args, param.color))) {
        if (nArgs < 3) {
            width = height = 1;
        } else {
            long w, h;
            if (!variable_to_int(args + 1, &w) || w <= 0 || !variable_to_int(args + 2, &h) || h <= 0) {
                fprintf(stderr, "Error: texture: invalid size argument\n");
                return 0;
            }
            width = w;
            height = h;
        }
        if (nArgs >= 4) {
            long c;
            if (!variable_to_int(args + 3, &c) || c <= 0 || c > 4 || ((unsigned int)c) > channels) {
                fprintf(stderr, "Error: texture: invalid channels argument\n");
                return 0;
            }
            channels = c;
        }
        if (width > UINT_MAX / height || width * height > UINT_MAX / channels
         || !(d = malloc(width * height * channels))) {
            fprintf(stderr, "Error: memory allocation failed\n");
            return 0;
        }
        for (i = 0; i < channels; i++) {
            if (param.color[i] > 1) {
                d[i] = 255;
            } else if (param.color[i] < 0) {
                d[i] = 0;
            } else {
                d[i] = 255.0 * param.color[i];
            }
        }
        for (i = 1; i < width * height; i++) {
            memcpy(d + i * channels, d, channels);
        }
        if ((description = malloc(22))) {
            char* p = description;
            int k;
            *p++ = '(';
            for (i = 0; i < channels; i++) {
                if ((k = sprintf(p, "%u, ", (unsigned int)(d[i]))) > 0 && k <= 5) p += k;
            }
            if (p - description > 2) {
                p[-2] = ')';
                p[-1] = 0;
            }
        }
    } else {
        fprintf(stderr, "Error: texture: invalid 1 path/color\n");
        return 0;
    }
    if (!(r = shell_new_texture(shell, d, width, height, channels, 4))) {
        free(d);
        return 0;
    }
    free(d);
    ((struct TextureData*)(r->data))->description = description;
    variable_make_struct(&structTexture, r, ret);
    return 1;
}

static int checkerboard(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Vec4 color[2];
    unsigned char d[16];
    char* description = NULL;
    struct ShellRefCount* r;
    struct DemoShell* shell = data;
    unsigned int i, j, k, c;

    if (nArgs == 0) {
        Vec4 black =  {0, 0, 0, 1};
        Vec4 purple = {1, 0, 1, 1};
        memcpy(color[0], black, sizeof(color[0]));
        memcpy(color[1], purple, sizeof(color[1]));
    } else if (nArgs == 2) {
        int a1, a2;
        if (((a1 = variable_to_vec4(args, color[0])) || variable_to_vec3(args, color[0]))
         && ((a2 = variable_to_vec4(args + 1, color[1])) || variable_to_vec3(args + 1, color[1]))) {
            if (!a1) color[0][3] = 1;
            if (!a2) color[1][3] = 1;
        } else {
            fprintf(stderr, "Error: invalid arguments for checkerboard()\n");
            return 0;
        }
    } else {
        fprintf(stderr, "Error: expected 0 or 2 arguments for checkerboard()\n");
        return 0;
    }
    k = 0;
    for (i = 0; i < 4; i++) {
        c = ((i + 1) >> 1) & 1;
        for (j = 0; j < 4; j++) {
            float f = color[c][j];
            if (f > 1) {
                d[k++] = 255;
            } else if (f < 0) {
                d[k++] = 0;
            } else {
                d[k++] = 255.0 * f;
            }
        }
    }
    if (!(r = shell_new_texture(shell, d, 2, 2, 4, 4))) {
        return 0;
    }
    if ((description = malloc(13))) {
        strcpy(description, "checkerboard");
    }
    ((struct TextureData*)(r->data))->description = description;
    variable_make_struct(&structTexture, r, ret);
    return 1;
}

struct PixelData {
    struct ShellRefCount* texture;
    unsigned int x, y;
};

static int pixel_get(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    const struct PixelData* pd = r->data;
    const struct TextureData* td = pd->texture->data;
    unsigned char* ptr = ((unsigned char*)(td + 1)) + td->c * (td->w * pd->y + pd->x);
    float* dptr;
    unsigned int i;
    switch (td->c) {
        case 1: dest->type = FLOAT; dptr = &dest->data.dfloat; break;
        case 2: dest->type = VEC2; dptr = dest->data.dvec2; break;
        case 3: dest->type = VEC3; dptr = dest->data.dvec3; break;
        case 4: dest->type = VEC4; dptr = dest->data.dvec4; break;
        default: return 0;
    }
    for (i = 0; i < td->c; i++) {
        *dptr++ = ((float)(*ptr++)) / 255.0f;
    }
    return 1;
}

static int pixel_set(const struct Variable* src, void* dest) {
    Vec4 value;
    const struct ShellRefCount* r = dest;
    const struct PixelData* pd = r->data;
    const struct TextureData* td = pd->texture->data;
    unsigned char* data = ((unsigned char*)(td + 1)) + td->c * (td->w * pd->y + pd->x), *ptr = data;
    unsigned int i;
    GLenum format;
    switch (td->c) {
        case 1: if (!variable_to_float(src, value)) return 0; format = GL_RED; break;
        case 2: if (!variable_to_vec2(src, value)) return 0; format = GL_RG; break;
        case 3: if (!variable_to_vec3(src, value)) return 0; format = GL_RGB; break;
        case 4: if (!variable_to_vec4(src, value)) return 0; format = GL_RGBA; break;
        default: return 0;
    }
    for (i = 0; i < td->c; i++) {
        float v = value[i] * 255.0f;
        if (v > 255.0f) v = 255.0f;
        if (v < 0.0f) v = 0.0f;
        *ptr++ = v;
    }
    /* TODO: not efficient in case it's called repetitively, could mark the pixel dirty and update all pixels at once before rendering */
    if (pthread_mutex_lock(&r->shell->swapMutex)) return 0;
    viewer_make_current(r->shell->viewer);
    glBindTexture(GL_TEXTURE_2D, td->tex);
    glTexSubImage2D(GL_TEXTURE_2D, 0, pd->x, pd->y, 1, 1, format, GL_UNSIGNED_BYTE, data);
    viewer_make_current(NULL);
    pthread_mutex_unlock(&r->shell->swapMutex);
    return 1;
}

static int pixel_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dpointer.gdata)) return 0;
    *dest = *src;
    return 1;
}

static void pixel_free(struct Variable* v) {
    shell_decref(v->data.dpointer.gdata);
}

static const struct PointerInfo ptrPixel = {pixel_get, pixel_set, pixel_copy, pixel_free};

static void pixel_destroy(struct DemoShell* shell, void* data) {
    if (!data) return;
    shell_decref(((struct PixelData*)data)->texture);
    free(data);
}

static int pixel(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct TextureData* td;
    struct PixelData* pd;
    struct ShellRefCount* r;
    long x, y;
    if (nArgs != 3) {
        fprintf(stderr, "Error: pixel() expects 3 arguments\n");
        return 0;
    }
    if (!variable_copy_dereference(args, ret)) {
        fprintf(stderr, "Error: failed to copy argument\n");
        return 0;
    }
    if (!variable_is_texture(ret) || !(td = variable_to_texture(ret))
     || !variable_to_int(args + 1, &x) || x < 0 || x > ((long)td->w)
     || !variable_to_int(args + 2, &y) || y < 0 || y > ((long)td->h)) {
        fprintf(stderr, "Error: invalid argument in pixel()\n");
        variable_free(ret);
        return 0;
    }
    if (!(r = shell_new_refcount(shell, pixel_destroy, NULL)) || !(pd = malloc(sizeof(*pd)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        variable_free(ret);
        if (r) shell_decref(r);
        return 0;
    }
    r->data = pd;
    pd->texture = ret->data.dstruct.data;
    pd->x = x;
    pd->y = y;
    variable_make_ptr(&ptrPixel, r, r, ret);
    return 1;
}

int shell_make_texture_vars(struct DemoShell* shell) {
    return builtin_function_with_data(shell, "texture", texture, shell)
        && builtin_function_with_data(shell, "checkerboard", checkerboard, shell)
        && builtin_function_with_data(shell, "pixel", pixel, shell);
}

int variable_is_texture(const struct Variable* var) {
    return var->type == STRUCT && var->data.dstruct.info == &structTexture;
}

struct TextureData* variable_to_texture(const struct Variable* var) {
    if (variable_is_texture(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        return r->data;
    }
    return NULL;
}

struct ShellRefCount* variable_to_texture_ref(const struct Variable* var) {
    if (variable_is_texture(var)) {
        return var->data.dstruct.data;
    }
    return NULL;
}

int variable_make_texture(struct DemoShell* shell, GLuint tex, const char* description, struct Variable* dest) {
    struct ShellRefCount* r;
    struct TextureData* td;
    unsigned int i;

    for (i = 0; i < shell->numRefcounts; i++) {
        if (shell->refcount[i] && shell->refcount[i]->destroy == tex_destroy_ && (td = shell->refcount[i]->data) && td->tex == tex) {
            if (!shell_incref(shell->refcount[i])) {
                return 0;
            }
            variable_make_struct(&structTexture, shell->refcount[i], dest);
            return 1;
        }
    }

    if (!(r = shell_new_refcount(shell, tex_destroy_, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return 0;
    }
    if (!(td = malloc(sizeof(*td)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        return 0;
    }
    r->data = td;
    if (description) {
        if ((td->description = malloc(strlen(description) + 1))) {
            strcpy(td->description, description);
        }
    } else {
        td->description = NULL;
    }
    td->w = 0;
    td->h = 0;
    td->c = 0;
    td->a = 0;
    td->tex = tex;
    td->interp = 0;
    variable_make_struct(&structTexture, r, dest);
    return 1;
}
