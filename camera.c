#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3dmr/render/camera.h>
#include <3dmr/render/camera_buffer_object.h>
#include <3dmr/render/viewer.h>
#include "builtins.h"
#include "shell.h"
#include "camera.h"

int cam_ptr_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dpointer.sdata)) return 0;
    variable_make_ptr(src->data.dpointer.info, src->data.dpointer.gdata, src->data.dpointer.sdata, dest);
    return 1;
}

void cam_ptr_free(struct Variable* v) {
    shell_decref(v->data.dpointer.sdata);
}

static int camera_position(const void* src, struct Variable* dest) {
    const struct Camera* c = src;
    camera_get_position(MAT_CONST_CAST(c->view), dest->data.dvec3);
    dest->type = VEC3;
    return 1;
}

static int camera_set_position(const struct Variable* src, void* dest) {
    struct ShellRefCount* r = dest;
    struct Camera *active, *camera = r->data;
    struct Node* node = *(struct Node**)(camera + 1);
    if (node) {
        if (!variable_to_vec3(src, node->position)) {
            return 0;
        }
        node->changedFlags |= POSITION_CHANGED;
    } else {
        Quaternion orientation;
        Vec3 position;
        if (!variable_to_vec3(src, position)) {
            return 0;
        }
        camera_get_orientation(MAT_CONST_CAST(camera->view), orientation);
        camera_view(position, orientation, camera->view);
        if ((active = variable_to_camera(&r->shell->activeCamera)) && active == camera) {
            camera_buffer_object_update_view(&r->shell->scene.camera, MAT_CONST_CAST(camera->view));
            camera_buffer_object_update_position(&r->shell->scene.camera, position);
            r->shell->cameraChanged = 1;
        }
    }
    return 1;
}

static const struct PointerInfo camPosPtr = {camera_position, camera_set_position, cam_ptr_copy, cam_ptr_free};

static int camera_ptr_position(const void* src, struct Variable* dest) {
    struct ShellRefCount* r = (void*)src;
    struct Camera* camera = r->data;
    if (!shell_incref(r)) return 0;
    variable_make_ptr(&camPosPtr, camera, r, dest);
    return 1;
}

static int camera_orientation(const void* src, struct Variable* dest) {
    const struct Camera* c = src;
    camera_get_orientation(MAT_CONST_CAST(c->view), dest->data.dquaternion);
    dest->type = QUATERNION;
    return 1;
}

static int camera_set_orientation(const struct Variable* src, void* dest) {
    struct ShellRefCount* r = dest;
    struct Camera *active, *camera = r->data;
    struct Node* node = *(struct Node**)(camera + 1);
    if (node) {
        if (!variable_to_quaternion(src, node->orientation)) {
            return 0;
        }
        node->changedFlags |= ORIENTATION_CHANGED;
    } else {
        Quaternion orientation;
        Vec3 position;
        camera_get_position(MAT_CONST_CAST(camera->view), position);
        if (!variable_to_quaternion(src, orientation)) {
            return 0;
        }
        camera_view(position, orientation, camera->view);
        if ((active = variable_to_camera(&r->shell->activeCamera)) && active == camera) {
            camera_buffer_object_update_view(&r->shell->scene.camera, MAT_CONST_CAST(camera->view));
            r->shell->cameraChanged = 1;
        }
    }
    return 1;
}

static const struct PointerInfo camOrientPtr = {camera_orientation, camera_set_orientation, cam_ptr_copy, cam_ptr_free};

static int camera_ptr_orientation(const void* src, struct Variable* dest) {
    struct ShellRefCount* r = (void*)src;
    struct Camera* camera = r->data;
    if (!shell_incref(r)) return 0;
    variable_make_ptr(&camOrientPtr, camera, r, dest);
    return 1;
}

static int camera_right(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    struct Camera* camera = r->data;
    dest->type = VEC3;
    camera_get_right(MAT_CONST_CAST(camera->view), dest->data.dvec3);
    return 1;
}

static int camera_left(const void* src, struct Variable* dest) {
    if (camera_right(src, dest)) {
        neg3v(dest->data.dvec3);
        return 1;
    }
    return 0;
}

static int camera_up(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    struct Camera* camera = r->data;
    dest->type = VEC3;
    camera_get_up(MAT_CONST_CAST(camera->view), dest->data.dvec3);
    return 1;
}

static int camera_down(const void* src, struct Variable* dest) {
    if (camera_up(src, dest)) {
        neg3v(dest->data.dvec3);
        return 1;
    }
    return 0;
}

static int camera_backward(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    struct Camera* camera = r->data;
    dest->type = VEC3;
    camera_get_backward(MAT_CONST_CAST(camera->view), dest->data.dvec3);
    return 1;
}

static int camera_forward(const void* src, struct Variable* dest) {
    if (camera_backward(src, dest)) {
        neg3v(dest->data.dvec3);
        return 1;
    }
    return 0;
}

static int camera_get_view(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    struct Camera* camera = r->data;
    dest->type = MAT4;
    memcpy(dest->data.dmat4, camera->view, sizeof(Mat4));
    return 1;
}

#define MEMBER_PROJECTION(name) \
static int camera_##name(const void* src, struct Variable* dest) { \
    const struct Camera* c = src; \
    const struct Node** n = (void*)(c + 1); \
    const int* hasProj = (void*)(n + 1); \
    const struct Projection* proj = (void*)(hasProj + 1); \
    if (*hasProj) { \
        variable_make_float(proj->name, dest); \
    } else { \
        dest->type = VOID; \
    } \
    return 1; \
} \
static int camera_set_##name##_(const struct Variable* src, void* dest) { \
    struct ShellRefCount* r = dest; \
    struct Camera *active, *camera = r->data; \
    struct Node** node = (void*)(camera + 1); \
    const int* hasProj = (void*)(node + 1); \
    struct Projection* proj = (void*)(hasProj + 1); \
    float val; \
    if (!*hasProj || !variable_to_float(src, &val)) return 0; \
    proj->name = val; \
    camera_projection(proj->ratio, proj->fov, proj->zNear, proj->zFar, camera->projection); \
    if ((active = variable_to_camera(&r->shell->activeCamera)) && active == camera) { \
        camera_buffer_object_update_projection(&r->shell->scene.camera, MAT_CONST_CAST(camera->projection)); \
        r->shell->cameraChanged = 1; \
    } \
    return 1; \
} \
static const struct PointerInfo cam##name##Ptr = {camera_##name, camera_set_##name##_, cam_ptr_copy, cam_ptr_free}; \
static int camera_ptr_##name(const void* src, struct Variable* dest) { \
    struct ShellRefCount* r = (void*)src; \
    struct Camera* camera = r->data; \
    if (!shell_incref(r)) return 0; \
    variable_make_ptr(&cam##name##Ptr, camera, r, dest); \
    return 1; \
}

MEMBER_PROJECTION(ratio)
MEMBER_PROJECTION(fov)
MEMBER_PROJECTION(zNear)
MEMBER_PROJECTION(zFar)

static int camera_get_projection(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    struct Camera* camera = r->data;
    dest->type = MAT4;
    memcpy(dest->data.dmat4, camera->projection, sizeof(Mat4));
    return 1;
}

static const struct FieldInfo structCameraFields[] = {
    {"position", camera_ptr_position},
    {"orientation", camera_ptr_orientation},
    {"left", camera_left},
    {"right", camera_right},
    {"down", camera_down},
    {"up", camera_up},
    {"backward", camera_backward},
    {"forward", camera_forward},
    {"view", camera_get_view},
    {"ratio", camera_ptr_ratio},
    {"fov", camera_ptr_fov},
    {"zNear", camera_ptr_zNear},
    {"zFar", camera_ptr_zFar},
    {"projection", camera_get_projection},
    {0}
};

static int cam_copy_(const struct Variable* src, struct Variable* dest) {
    void* data = src->data.dstruct.data;
    if (!shell_incref(data)) return 0;
    variable_make_struct(src->data.dstruct.info, data, dest);
    return 1;
}

static void cam_free_(void* data) {
    shell_decref(data);
}

static const struct StructInfo structCamera = {"Camera", structCameraFields, cam_copy_, cam_free_};

static void camera_destroy_(struct DemoShell* shell, void* d) {
    free(d);
}

static struct ShellRefCount* shell_new_camera(struct DemoShell* shell, const struct Camera* camera, const struct Projection* proj) {
    struct ShellRefCount* r;
    struct Camera* c = NULL;
    struct Node** node;
    int* hasProj;
    struct Projection* projection;

    if (!(r = shell_new_refcount(shell, camera_destroy_, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return NULL;
    }
    if (!(c = malloc(sizeof(*c) + sizeof(*node) + sizeof(*hasProj) + sizeof(*projection)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        return NULL;
    }
    node = (void*)(c + 1);
    hasProj = (void*)(node + 1);
    projection = (void*)(hasProj + 1);
    *c = *camera;
    *node = NULL;
    if ((*hasProj = (proj != NULL))) {
        *projection = *proj;
    }
    r->data = c;
    return r;
}

static int camera_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Camera camera;
    struct Projection proj;
    Quaternion orientation;
    Vec3 position;
    float ratio, fov, zNear, zFar;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;

    if (nArgs != 5) {
        fprintf(stderr, "Error: camera() needs 5 arguments (position, orientation, fov, zNear, zFar)\n");
        return 0;
    }
    if (!variable_to_vec3(args++, position)
     || !variable_to_quaternion(args++, orientation)
     || !variable_to_float(args++, &fov)
     || !variable_to_float(args++, &zNear)
     || !variable_to_float(args++, &zFar)) {
        fprintf(stderr, "Error: camera() invalid argument\n");
        return 0;
    }
    ratio = ((float)shell->viewer->width) / ((float)shell->viewer->height);
    camera_projection(ratio, fov, zNear, zFar, camera.projection);
    camera_view(position, orientation, camera.view);
    proj.ratio = ratio;
    proj.fov = fov;
    proj.zFar = zFar;
    proj.zNear = zNear;
    if (!(r = shell_new_camera(shell, &camera, &proj))) {
        return 0;
    }
    variable_make_struct(&structCamera, r, ret);
    return 1;
}

static int activecam_ptr_get(const void* src, struct Variable* dest) {
    const struct DemoShell* shell = src;
    return variable_copy(&shell->activeCamera, dest);
}

static int activecam_ptr_set(const struct Variable* src, void* dest) {
    struct Variable old, new;
    struct DemoShell* shell = dest;
    struct Camera* c;
    if (!variable_copy_dereference(src, &new)) return 0;
    if (!variable_is_camera(&new)) {
        variable_free(&new);
        return 0;
    }
    old = shell->activeCamera;
    shell->activeCamera = new;
    shell->cameraChanged = 1;
    c = variable_to_camera(&new);
    camera_set_ratio(((float)shell->viewer->width) / ((float)shell->viewer->height), c->projection);
    camera_buffer_object_update_projection(&shell->scene.camera, MAT_CONST_CAST(c->projection));
    camera_buffer_object_update_view_and_position(&shell->scene.camera, MAT_CONST_CAST(c->view));
    variable_free(&old);
    return 1;
}

static const struct PointerInfo activecam = {activecam_ptr_get, activecam_ptr_set, NULL, NULL};

int shell_make_camera(struct DemoShell* shell) {
    Quaternion orientation = {1, 0, 0, 0};
    Vec3 position = {0, 0, 10};
    struct Camera camera;
    struct Projection proj;
    struct ShellRefCount* r;
    proj.ratio = ((float)shell->viewer->width) / ((float)shell->viewer->height);
    proj.fov = 1.04;
    proj.zNear = 0.1;
    proj.zFar = 2000;
    camera_projection(proj.ratio, proj.fov, proj.zNear, proj.zFar, camera.projection);
    camera_view(position, orientation, camera.view);
    camera_buffer_object_update_projection(&shell->scene.camera, MAT_CONST_CAST(camera.projection));
    camera_buffer_object_update_view_and_position(&shell->scene.camera, MAT_CONST_CAST(camera.view));
    if (!(r = shell_new_camera(shell, &camera, &proj))) {
        return 0;
    }
    variable_make_struct(&structCamera, r, &shell->activeCamera);
    shell->cameraChanged = 1;
    return builtin_function_with_data(shell, "camera", camera_, shell)
        && builtin_ptr(shell, "activecam", &activecam, shell, shell);
}

int variable_is_camera(const struct Variable* var) {
    return var->type == STRUCT && var->data.dstruct.info == &structCamera;
}

struct Camera* variable_to_camera(const struct Variable* var) {
    if (variable_is_camera(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        return r->data;
    }
    return NULL;
}

struct Node* variable_to_camera_node(const struct Variable* var) {
    if (variable_is_camera(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct Camera* c = r->data;
        return *(struct Node**)(c + 1);
    }
    return NULL;
}

struct Node** variable_to_camera_node_ptr(const struct Variable* var) {
    if (variable_is_camera(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct Camera* c = r->data;
        return (struct Node**)(c + 1);
    }
    return NULL;
}

int variable_set_camera_node(struct Variable* var, struct Node* node) {
    if (variable_is_camera(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct Camera* c = r->data;
        struct Node** n = (struct Node**)(c + 1);
        if (*n && node) return 0;
        *n = node;
        if (node) {
            camera_get_position(MAT_CONST_CAST(c->view), node->position);
            camera_get_orientation(MAT_CONST_CAST(c->view), node->orientation);
            node->changedFlags |= POSITION_CHANGED | ORIENTATION_CHANGED;
        }
        return 1;
    }
    return 0;
}

struct Projection* variable_to_camera_proj(const struct Variable* var) {
    if (variable_is_camera(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct Camera* c = r->data;
        struct Node** n = (void*)(c + 1);
        int* hasProj = (void*)(n + 1);
        return (*hasProj) ? ((void*)(hasProj + 1)) : (NULL);
    }
    return NULL;
}

int variable_make_camera(struct DemoShell* shell, const struct Camera* camera, struct Variable* dest) {
    struct ShellRefCount* r;

    if (!(r = shell_new_camera(shell, camera, NULL))) {
        return 0;
    }
    variable_make_struct(&structCamera, r, dest);
    return 1;
}
