#include "shell.h"

#ifndef TDSH_CAMERA_H
#define TDSH_CAMERA_H

struct Projection {
    float ratio, fov, zNear, zFar;
};

int shell_make_camera(struct DemoShell* shell);

int variable_is_camera(const struct Variable* var);
struct Camera* variable_to_camera(const struct Variable* var);
struct Node* variable_to_camera_node(const struct Variable* var);
struct Node** variable_to_camera_node_ptr(const struct Variable* var);
int variable_set_camera_node(struct Variable* var, struct Node* node);
struct Projection* variable_to_camera_proj(const struct Variable* var);
int variable_make_camera(struct DemoShell* shell, const struct Camera* camera, struct Variable* dest);

#endif
