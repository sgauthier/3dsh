#include "help.h"

#define F(func) "\x1B[1m" #func "\x1B[0m"
#define T(type) "\x1B[34;1m" #type "\x1B[0m "
#define I(identifier) "\x1B[36m\x1B[4m" #identifier "\x1B[0m"
#define V(type, name) T(type) I(name)
#define H(name, doc) {#name, doc}
#define N "\n"
#define P "\n\t"
#define C ", "
#define OPT(str) "\x1B[33;1m[\x1B[0m" str "\x1B[33;1m]\x1B[0m"
#define END {0, 0}

#define CONST T(const)
#define VOID T(void)
#define INT T(int)
#define VINT(name) V(int, name)
#define FLOAT T(float)
#define VFLOAT(name) V(float, name)
#define VEC2 T(vec2)
#define VVEC2(name) V(vec2, name)
#define VEC3 T(vec3)
#define VVEC3(name) V(vec3, name)
#define VEC4 T(vec4)
#define VVEC4(name) V(vec4, name)
#define MAT2 T(mat2)
#define VMAT2(name) V(mat2, name)
#define MAT3 T(mat3)
#define VMAT3(name) V(mat3, name)
#define MAT4 T(mat4)
#define VMAT4(name) V(mat4, name)
#define QUATERNION T(quaternion)
#define VQUATERNION(name) V(quaternion, name)
#define STRING T(string)
#define VSTRING(name) V(string, name)

#define SFUNC1(func) FLOAT F(func) "(" VFLOAT(x) ")"
#define SFUNC2(func) FLOAT F(func) "(" VFLOAT(x) C VFLOAT(y) ")"

const struct Help help[] = {
    /* Functions
     ***********/
    H(abs, SFUNC1(abs) P "Return the absolute value of " I(x) "."),
    H(abstime, INT F(abstime) "()" P "Return the number of seconds since the Epoch."),
    H(acos, SFUNC1(acos) P "Return the arccosinus of " I(x) "."),
    H(add, INT F(add) "(" V(array, a) C V(var, v) ")" P "Add (append) " I(v) " to the array " I(a) ". Return the index of the new value."),
    H(angle, FLOAT F(angle) "(" VQUATERNION(q) ")" P "Return the angle of the rotation quaternion " I(q) "."),
    H(asin, SFUNC1(asin) P "Return the arcsinus of " I(x) "."),
    H(atan, SFUNC1(atan) P "Return the arctangent of " I(x) "."),
    H(atan2, SFUNC2(atan2) P "Return the arctangent of " I(y) "/" I(x) "."),
    H(axis, FLOAT F(axis) "(" VQUATERNION(q) ")" P "Return the axis of the rotation quaternion " I(q) "."),
    H(bitangent, VEC3 F(bitangent) "(" V(Mesh, m) C VINT(i) ")" P "Return the bitangent of the " I(i) "-th vertex (assignable)."),
    H(box,  T(Mesh) F(box) "(" VVEC3(size) ")"
           N T(Mesh) F(box) "(" VFLOAT(w) C VFLOAT(h) C VFLOAT(d) ")"
           P "Return a box mesh."),
    H(camera, T(Camera) F(camera) "(" VVEC3(position) C VQUATERNION(orientation) C VFLOAT(fov) C I(zNear) C I(zFar) ")" P "Return a camera object with the specified values."),
    H(chdir,  VOID F(chdir) "()" P "Change the current directory to the current file's directory."
            N VOID F(chdir) "(" VSTRING(path) ")" P "Change the current directory to the specified path."),
    H(checkerboard, T(Texture) F(checkerboard) "(" OPT(V(vec3/4, c1) "=black" C V(vec3/4, c2) "=magenta") ")" P "Return a checkerboard texture with two colors."),
    H(circle, T(Mesh) F(circle) "(" VFLOAT(radius) C VINT(nVerts) ")" P "Return a circle mesh."),
    H(clamp, FLOAT F(clamp) "(" VFLOAT(x) C VFLOAT(a) C VFLOAT(b) ")" P "Return x clamped to the interval [a, b]."),
    H(compute_tangents, VOID F(compute_tangents) "(" V(Mesh, mesh) ")" P "Compute tangents and bitangents."),
    H(cone, T(Mesh) F(cone) "(" VFLOAT(depth) C VFLOAT(radius1) OPT(C VINT(nVerts) "=32" OPT(C VFLOAT(radius2) "=0")) ")" P "Returns a cone with " I(nVerts) " vertices on the side."),
    H(cos, SFUNC1(cos) P "Return the cosinus of " I(x) "."),
    H(cross, VEC3 F(cross) "(" VVEC3(u) C I(v) ")" P "Return cross product of the two vectors."),
    H(cube, T(Mesh) F(cube) "(" OPT(VFLOAT(size) "=1") ")" P "Same as " F(box) "(" I(size) C I(size) C I(size) ")."),
    H(cylinder, T(Mesh) F(cylinder) "(" VFLOAT(depth) C VFLOAT(radius) OPT(C VINT(nVerts) "=32") ")" P "Returns a cylinder with " I(nVerts) " vertices on the side."),
    H(del, VOID F(del) "(" V(array, a) C VINT(i) ")" P "Remove the " I(i) "-th variable from the array " I(a) "."),
    H(det, FLOAT F(det) "(" V(mat, m) ")" P "Return determinant of matrix."),
    H(dlight, T(Dlight) F(dlight) "(" VVEC3(color) C VVEC3(direction) ")" P "Create a directional light."),
    H(donut, T(Mesh) F(donut) "(" VFLOAT(majRadius) C VFLOAT(minRadius) OPT(C VINT(nMaj) "=48" C VINT(nMin) "=12" OPT(C VINT(textured))) ")" P "Returns a donut mesh with " I(nMaj) " points on the major circle, " I(nMin) " on the minor one."),
    H(dot, FLOAT F(dot) "(" V(vec, u) C I(v) ")" P "Return dot product of the two vectors."),
    H(enable_exec, VOID F(enable_exec) "(" VINT(v) ")" P "If v is 0, pause code execution. If v is positive, resume code execution. If v is negative, toggle whether code execution is enabled."),
    H(enable_render, VOID F(enable_render) "(" VINT(v) ")" P "If v is 0, pause rendering. If v is positive, resume rendering. If v is negative, toggle whether rendering is enabled."),
    H(error, VOID F(error) "(...)" P "Raise an error. The evaluation of the current statement is aborted. The arguments are printed like " F(print) ", prefixed by 'Error:'."),
#ifdef _POSIX_C_SOURCE
    H(exec, INT F(exec) "(" VSTRING(command) C "...)" P "Execute " I(command) " with the specified string arguments, and returns the exit code. An error is raised if a signal causes the termination of the command."),
#endif
    H(exit, VOID F(exit) "()" P "Exits the shell."),
    H(exp, SFUNC1(exp) P "Return e^" I(x) "."),
    H(export_obj, VOID F(export_obj) "(" V(Mesh, m) C VSTRING(path) ")" P "Export a mesh in the OBJ format."),
    H(export_ogex, VOID F(export_ogex) "(" V(T, var1) C "..." C VSTRING(path) ")" P "Export resources in the OpenGEX format, to the file pointed to " I(path) "."
            P "Support filetypes for " T(T) ": Node, Dlight, Plight, Camera, VertexArray, Mesh, Material."),
    H(frame, T(Node) F(frame) "()" P "Return a frame (3 colored arrows)."),
    H(grid, T(Node) F(grid) "(" VINT(nx) C I(ny) C VVEC2(spacing) C V(Mesh/VertexArray, geom) C V(Material, mat) ")" P "Create a nx x ny grid node with each cell containing the replicated geometry."),
    H(help,   VOID F(help) "()" P "Lists all help topics."
            N VOID F(help) "(" VSTRING(topic) ")" P "Print documentation for the specified topic."),
    H(hsv2rgb, VEC3 F(hsv2rgb) "(" VVEC3(hsv) ")" P "Convert color from HSV space to RGB space."),
    H(icosphere, T(Mesh) F(icosphere) "(" VFLOAT(radius) OPT(C VINT(n) "=4" OPT(C VSTRING(uvt) C VVEC3(uvp))) ")" P "Return an icosphere mesh. " I(n) " is the number of subdibisions."
            P I(uvt) " can be cylindric, mercator, miller, or equirect."),
    H(import_ogex, T(Node) F(import_ogex) "(" VSTRING(path) ")" P "Import a scene in OpenGEX format."),
    H(inv, T(mat) F(inv) "(" V(mat, m) ")" P "Return inverse of matrix."),
    H(lasterr, INT F(lasterr) "()" P "Return 0 if last top level expression was evaluated successfully, 1 otherwise."),
    H(len, INT F(len) "(" V(array, a) ")" P "Return the number of elements in a."),
    H(lerp, FLOAT F(lerp) "(" VFLOAT(a) C VFLOAT(b) C VFLOAT(t) ")" P "Return a + t * (b - a)."),
    H(log, SFUNC1(log) P "Return the natural logarithm of " I(x) "."),
    H(mat2,   MAT2 F(mat2) "()" P "Return a zero " MAT2 "."
            N MAT2 F(mat2) "(" VFLOAT(f) ")" P "Return a " MAT2 "with " I(f) " on the diagonal."
            N MAT2 F(mat2) "(" VVEC2(c1) C VVEC2(c2) ")" P "Return a " MAT2 "with specified columns."
            N MAT2 F(mat2) "(" VFLOAT(e1) C I(e2) C I(e3) C I(e4) ")" P "Return a " MAT2 "with the specified elements."),
    H(mat3, "Construct a " MAT3 ". See mat2 for usage."),
    H(mat4, "Construct a " MAT4 ". See mat2 for usage."),
    H(mesh, T(Mesh) F(mesh) "(" VSTRING(path) ")" P "Load an OBJ mesh from " I(path) "."),
    H(mtrand, T(MTRand) F(mtrand) "(" VINT(seed) ")" P "Return a MT random generator."),
    H(node,   T(Node) F(node) "()" P "Return a new empty node."
            N T(Node) F(node) "(" V(Mesh/VertexArray, geom) C V(Material, mat) ")" P "Return a geometry node."
            N T(Node) F(node) "(" V(Camera, cam) ")" P "Return a camera node."
            N T(Node) F(node) "(" V(Dlight/Plight, cam) ")" P "Return a light node."),
    H(norm, FLOAT F(norm) "(" V(T, v) ")" P "Return norm of vector or quaternion."),
    H(normal, VEC3 F(normal) "(" V(Mesh, m) C VINT(i) ")" P "Return the normal of the " I(i) "-th vertex (assignable)."),
    H(normalize, T(T) F(normalize) "(" V(T, v) ")" P "Normalize vector or quaternion."),
    H(pbr, T(Material) F(pbr) "(" V(vec3/Texture, albedo) C V(float/Texture, metalness) C V(float/Texture, roughness) OPT(C V(Texture, normalmap)) ")" P "Return a PBR material."),
    H(phong, T(Material) F(phong) "(" OPT(V(vec3/Texture, ambient) "=black" C) V(vec3/Texture, diffuse) OPT(C V(vec3/Texture, specular) "=black" OPT(C V(float/Texture, shininess) "=1" OPT(C V(Texture, normalmap)))) ")" P "Return a Phong material."),
    H(pixel, T(vec/float) F(pixel) "(" V(Texture, t) C VINT(x) C VINT(y) ")" P "Return the value of the texture at the specified coordinates (assignable)."),
    H(plight, T(Plight) F(plight) "(" VVEC3(color) C VVEC3(position) C VFLOAT(radius) ")" P "Create a point light."),
    H(pow, SFUNC2(pow) P "Return x to the power y."),
    H(print, VOID F(print) "(...)" P "Prints a string representation of all the arguments, concatenated with no separator."),
    H(quad,   T(Mesh) F(quad) "(" VVEC2(size) ")"
            N T(Mesh) F(quad) "(" VFLOAT(w) C VFLOAT(h) ")"
            P "Return a quad mesh."),
    H(quaternion, QUATERNION F(quaternion) "()" P "Return the identity quaternion."
                N QUATERNION F(quaternion) "(" VQUATERNION(q) ")" P "Returns a copy of " I(q) "."
                N QUATERNION F(quaternion) "(" VVEC3(axis) C VFLOAT(angle) ")" P "Return a rotation quaternion."
                N QUATERNION F(quaternion) "(" VFLOAT(a) C I(b) C I(c) C I(d) ")" P "Return a quaternion with specified components."),
    H(randflt, FLOAT F(randflt) "(" OPT(V(MTRand, state)) ")" P "Returns a random float between 0 and 1."),
    H(randint, INT F(randint) "(" OPT(V(MTRand, state)) ")" P "Returns a random 32 bit unsigned integer."),
    H(randrange, INT F(randrange) "(" VINT(a) C I(b) OPT(C V(MTRand, state)) ")" P "Returns a random integer between " I(a) " and " I(b) "."),
    H(randseed, VOID F(randseed) "(" VINT(seed) OPT(C V(MTRand, state)) ")"P "Set random state."),
    H(reltime, FLOAT F(reltime) "()" P "Return the elapsed time since the shell was started, in seconds."),
    H(rot3, MAT3 F(rot3) "(" VQUATERNION(q) ")" P "Return a rotation matrix corresponding to " I(q) "."),
    H(rot4, MAT4 F(rot4) "(" VQUATERNION(q) ")" P "Return a rotation matrix corresponding to " I(q) "."),
    H(rotation,   QUATERNION F(rotation) "(" VMAT3(m) ")"
                N QUATERNION F(rotation) "(" VMAT3(m) ")"
                P "Convert rotation matrix " I(m) " to a rotation quaternion. The results are unspecified if the 3x3 part of " I(m) " is not a valid rotation matrix."
                N QUATERNION F(rotation) "(" VVEC3(u) C I(v) ")" P "Return a quaternion with the rotation that transforms " I(u) " into " I(v) ". Error if opposed vectors."),
    H(screenshot, VOID F(screenshot) "(" VSTRING(path) ")" P "Save a screenshot to " I(path) " in PNG format."),
    H(sin, SFUNC1(sin) P "Return the sinus of " I(x) "."),
    H(skybox, VOID F(skybox) "()"
            P "Unload current skybox."
            N VOID F(skybox) "(" VSTRING(hdrEquirectTexturePath) ")"
            N VOID F(skybox) "(" VSTRING(pngFace1Path) C "..." C VSTRING(pngFace6Path) ")"
            P "Load a skybox from file(s)."),
    H(slerp, QUATERNION F(slerp) "(" VQUATERNION(a) C VQUATERNION(b) C VFLOAT(t) ")" P "Return the spherical interpolation (slerp) between " I(a) " and " I(b) "."),
    H(slight, T(Slight) F(slight) "(" VVEC3(color) C VVEC3(position) C VVEC3(direction) C VFLOAT(intensity) C VFLOAT(outerAngle) C VFLOAT(innerAngle) ")" P "Create a spot light."),
    H(smootherstep, SFUNC1(smootherstep) P "Return 6" I(x) "^5 - 15" I(x) "^4 + 10" I(x) "^3."),
    H(smoothstep, SFUNC1(smoothstep) P "Return -2" I(x) "^3 + 3" I(x) "^2."),
    H(solid, T(Material) F(solid) "(" V(vec3/Texture, color) OPT(C VINT(overlay) "=0") ")" P "Return a solid color/texture material."),
    H(sprintf, STRING F(sprintf) "(" VSTRING(format) C "...)" P "Return a string formated according to the specified format and with the provided arguments. Possible format codes are %d (int), %f, %g (float), and %s (string)."),
    H(sqrt, SFUNC1(sqrt) P "Return the square root of " I(x) "."),
    H(swing, QUATERNION F(swing) "(" VQUATERNION(q) C VVEC3(dir) ")" P "Extracts the rotation part not along the specified direction."),
    H(system, INT F(system) "(" VSTRING(command) P "Executes a shell command, and return its exit code."),
    H(tan, SFUNC1(tan) P "Return the tangent of " I(x) "."),
    H(tangent, VEC3 F(tangent) "(" V(Mesh, m) C VINT(i) ")" P "Return the tangent of the " I(i) "-th vertex (assignable)."),
    H(texcoord, VEC3 F(texcoord) "(" V(Mesh, m) C VINT(i) ")" P "Return the texture coordinates of the " I(i) "-th vertex (assignable)."),
    H(texture, T(Texture) F(texture) "(" VSTRING(path) ")" P "Load a texture from a PNG file."
             N T(Texture) F(texture) "(" V(vec/float, color) OPT(C VINT(w) "=1" C VINT(h) "=1" OPT(C VINT(channels))) ")" P "Create an texture of size (" I(w) C I(h) ") filled with " I(color) "."),
    H(timer, T(func) F(timer) "()" P "Return a function which, when called, returns the elapsed time since last call. The returned function takes no arguments."),
    H(title, VOID F(title) "(" VSTRING(t) ")" P "Sets the title of the window."),
    H(tmpprint, VOID F(tmpprint) "(...)" P "Like " F(print) ", but first erases the current line and with no carriage return at the end."),
    H(tr, T(mat) F(tr) "(" V(mat, m) ")" P "Return transpose of matrix."),
    H(trace, FLOAT F(trace) "(" V(mat, m) ")" P "Return trace of matrix."),
    H(twist, QUATERNION F(twist) "(" VQUATERNION(q) C VVEC3(dir) ")" P "Extracts the rotation part along the specified direction."),
    H(uvsphere, T(Mesh) F(uvsphere) "(" VFLOAT(radius) OPT(C VINT(nLat) "=16" C VINT(nLong) "=32" OPT(C VSTRING(uvt) C VVEC3(uvp))) ")" P "Return an uvsphere mesh. " I(nLat) " and " I(nLong) " are the number of latitudes/longitudes."
            P I(uvt) " can be cylindric, mercator, miller, or equirect."),
    H(vec2,   VEC2 F(vec2) "()" P "Return a zero " VEC2 "."
            N VEC2 F(vec2) "(" VFLOAT(f) ")" P "Return a " VEC2 " with all components initialized with " I(f) "."
            N VEC2 F(vec2) "(" VVEC2(v) ")" P "Return a copy of " I(v) "."
            N VEC2 F(vec2) "(" VFLOAT(e1) C VFLOAT(e2) ")" P "Return (" I(e1) C I(e2) ")."),
    H(vec3, "Construct a " VEC3 ". See vec2 for usage."),
    H(vec4, "Construct a " VEC4 ". See vec2 for usage."),
    H(vertex, VEC3 F(vertex) "(" V(Mesh, m) C VINT(i) ")" P "Return the position of the " I(i) "-th vertex (assignable)."),
    H(vertex_array, T(VertexArray) F(vertex_array) "(" V(Mesh, m) ")" P "Transfert mesh to GPU (Vertex Array)."),
    H(vsync, VOID F(vsync) "()" P "Wait for the next frame to be rendered."),
    H(wait, VOID F(wait) "(" VFLOAT(s) ")" P "Waits s seconds. Note that callbacks don't run during the wait period."),

    /* Variables
     ***********/
    H(activecam, V(Camera, activecam) P "The active camera. It can be wrapped in a node, eg 'activecam = n.camera'. It is also possible to wrap the default camera in a node 'node(activecam)'."),
    H(ambient, VVEC3(ambient) P "The ambient light color. Defaults to no ambient light, ie black."),
    H(backward, CONST VVEC3(backward) P "Positive z axis."),
    H(black, CONST VVEC3(black) P "Black/no color (0, 0, 0)."),
    H(blue, CONST VVEC3(blue) P "Blue color (0, 0, 1)."),
    H(cyan, CONST VVEC3(cyan) P "Cyan color (0, 1, 1)."),
    H(dlights, V(array, dlights) P "Contains the active directional lights. Can be modified with " F(add) " and " F(del) ", or by assigning a valid index to a " T(Dlight) "id member."),
    H(down, CONST VVEC3(down) P "Negative y axis."),
    H(forward, CONST VVEC3(forward) P "Negative z axis."),
    H(fps, CONST VFLOAT(fps) P "The current FPS. The value changes and is updated regularly, but it cannot be assigned."),
    H(green, CONST VVEC3(green) P "Green color (0, 1, 0)."),
    H(height, CONST VINT(height) P "Current viewer window height. Changes when the window is resized, but cannot be assigned."),
    H(keyCb, VOID I(keyCb) "(" VINT(key) C VINT(code) C VINT(action) C VINT(mods) ")" P "Keypress callback. " I(key) " is the virtual key, " I(code) " is the scancode. " I(action) " is 1 when the key is pressed, 2 when repeated, or 0 when released." P "The default callback moves the camera with arrows and PgDn and PgUp/WASDQE, exits on escape, and takes a screenshot on F12." P "See callbacks."),
    H(left, CONST VVEC3(left) P "Negative x axis."),
    H(magenta, CONST VVEC3(magenta) P "Magenta color (1, 0, 1)."),
    H(mouseCb, VOID I(mouseCb) "(" VFLOAT(x) C I(y) C I(dx) C I(dy) C VINT(btnLeft) C I(btnMiddle) C I(btnRight) ")" P "Mouse callback. (" I(x) C I(y) "): the position in pixels, from the top left corner. (" I(dx) C I(dy) "): the displacement since last call. The btn parameters: 1 if the corresponding button is pressed, 0 otherwise." P "The default callback rotates the camera." P "See callbacks."),
    H(multi, CONST VVEC3(multi) P "A color value that changes over time, but is not assignable. It can be used as material parameters to create color-changing materials."),
    H(numRenderedObjs, CONST VINT(numRenderedObjs) P "The number of objects drawn in a frame. The value changes and is updated at each frame, but it cannot be assigned."),
    H(pi, CONST VFLOAT(pi) P "The pi constant."),
    H(plights, V(array, plights) P "Contains the active point lights. Can be modified with " F(add) " and " F(del) ", or by assigning a valid index to a " T(Plight) "id member."),
    H(ps1, VSTRING(ps1) "The prompt string for a new command."),
    H(ps2, VSTRING(ps2) "The prompt string for a command continuation."),
    H(red, CONST VVEC3(red) P "Red color (1, 0, 0)."),
    H(resizeCb, VOID I(resizeCb) "(" VINT(width) C VINT(height) ")" P "Resize callback. (" I(width) C I(height) ") is the new window size." P "The default callback updates the projection ratio of the active camera." P "See callbacks."),
    H(right, CONST VVEC3(right) P "Positive x axis."),
    H(scene, V(Node, scene) P "A special node that is the root of the scene graph."),
    H(scrollCb, VOID I(scrollCb) "(" VFLOAT(x) C VFLOAT(y) ")" P "Scroll callback. (" I(x) C I(y) ") represents an amount of horizontal/vertical scroll." P "The default callback move the active camera in its look direction for vertical scrolls." P "See callbacks."),
    H(slights, V(array, slights) P "Contains the active spot lights. Can be modified with " F(add) " and " F(del) ", or by assigning a valid index to a " T(Slight) "id member."),
    H(up, CONST VVEC3(up) P "Positive y axis."),
    H(width, CONST VINT(width) P "Current viewer window width. Changes when the window is resized, but cannot be assigned."),
    H(white, CONST VVEC3(white) P "White color (1, 1, 1)."),
    H(yellow, CONST VVEC3(yellow) P "Yellow color (1, 1, 0)."),

    /* General
     *********/
    H(callbacks, "Callbacks are functions that handle events. See " I(keyCb) C I(mouseCb) C I(resizeCb) C I(scrollCb) " for event-specific details. These variables can be assigned custom functions. The default ones can be saved in other variables for use in custom handlers."),

    END
};
