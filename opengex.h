#include "shell.h"

#ifndef TDSH_OPENGEX_H
#define TDSH_OPENGEX_H

int shell_make_opengex_funcs(struct DemoShell* shell);

#endif
