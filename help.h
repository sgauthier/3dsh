#ifndef TDSH_HELP_H
#define TDSH_HELP_H

struct Help {
    const char *name, *doc;
};

extern const struct Help help[];

#endif
