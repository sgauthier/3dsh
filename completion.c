#include <stdlib.h>
#include "shell.h"

#ifdef READLINE
#include <stdio.h>
#include <readline/readline.h>

static const struct DemoShell* shell = NULL;

static char* symbol_generator(const char* s, int state) {
    static struct State {
        const struct ShellVarNode* node;
        unsigned int nextChild;
    } *st;
    static size_t slen;
    static unsigned int depth, maxdepth;
    struct State *cur, *tmp;
    char* ret = NULL;

    if (!shell) return NULL;
    if (!state) {
        if (shell->numContexts != 1) return NULL;
        if (!(st = malloc(sizeof(*st)))) return NULL;
        st->node = &shell->contexts[0];
        st->nextChild = 0;
        maxdepth = depth = 1;
        slen = strlen(s);
    }

    while (depth && !ret) {
        cur = st + depth - 1;
        if (cur->node->c == '/' || (depth > 1 && depth - 2 < slen && s[depth - 2] != cur->node->c)) {
            --depth;
        } else if (cur->nextChild < cur->node->numChildren) {
            if (depth < maxdepth) {
                tmp = st + depth++;
                tmp->node = &cur->node->value.children[cur->nextChild++];
                tmp->nextChild = 0;
            } else if ((tmp = realloc(st, (depth + 1) * sizeof(*st)))) {
                st = tmp;
                cur = st + depth - 1;
                tmp += depth++;
                tmp->node = &cur->node->value.children[cur->nextChild++];
                tmp->nextChild = 0;
            } else {
                --depth;
            }
        } else if (cur->node->c || depth <= 1) {
            --depth;
        } else {
            int isfunc = cur->node->value.var->type == FUNCTION;
            --depth;
            if ((ret = malloc(depth + isfunc))) {
                unsigned int i;
                for (i = 0; i < depth; i++) {
                    ret[i] = st[i + 1].node->c;
                }
                if (isfunc) {
                    ret[i - 1] = '(';
                    ret[i] = 0;
                }
            }
        }
    }
    if (!ret) {
        free(st);
    }
    return ret;
}

static char** readline_completion(const char* s, int start, int end) {
    int i, str = 0;

    for (i = 0; i <= start; i++) {
        str ^= (rl_line_buffer[i] == '\"' && (!i || rl_line_buffer[i - 1] != '\\'));
    }
    rl_attempted_completion_over = 1;
    rl_completion_append_character = 0;

    if (str) {
        return rl_completion_matches(s, rl_filename_completion_function);
    }
    return rl_completion_matches(s, symbol_generator);
}

static char breakChars[] = " #()[]{}+-*/<>=!&|,;.\"";

static char* wb_hook(void) {
    static char ret[sizeof(breakChars)];
    int i, str = 0;
    for (i = 0; i < rl_point; i++) {
        str ^= (rl_line_buffer[i] == '\"' && (!i || rl_line_buffer[i - 1] != '\\'));
    }
    if (str) {
        strcpy(ret, "\"");
    } else {
        strcpy(ret, breakChars);
    }
    return ret;
}

void shell_init_completion(struct DemoShell* s) {
    rl_attempted_completion_function = readline_completion;
    rl_basic_word_break_characters = breakChars;
    rl_completer_word_break_characters = breakChars;
    rl_basic_quote_characters = "\"";
    rl_completer_quote_characters = "\"";
    rl_completion_word_break_hook = wb_hook;
    shell = s;
}

#else

void shell_init_completion(struct DemoShell* s) {
}

#endif
