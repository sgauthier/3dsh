#include "shell.h"

#ifndef TDSH_MATERIAL_H
#define TDSH_MATERIAL_H

enum MaterialType {SOLID, PHONG, PBR, NOT_SUPPORTED_MAT};

struct MaterialData {
    enum MaterialType type;
    struct Material* material;
    unsigned int requiredFlags;
};

int shell_make_material_vars(struct DemoShell* shell);

int variable_is_material(const struct Variable* var);
struct Material* variable_to_material(const struct Variable* var);
struct MaterialData* variable_to_material_data(const struct Variable* var);
int variable_make_material(struct DemoShell* shell, struct Material* material, struct Variable* dest);

#endif
