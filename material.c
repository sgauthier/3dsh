#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3dmr/material/solid.h>
#include <3dmr/material/phong.h>
#include <3dmr/material/pbr.h>
#include "builtins.h"
#include "material.h"
#include "render.h"
#include "texture.h"

struct SolidMaterialParams2 {
    struct SolidMaterialParams solid;
    int overlay;
};

static unsigned int material_num_variables(enum MaterialType type) {
    switch (type) {
        case SOLID: return 1;
        case PHONG: return 5;
        case PBR: return 4;
        default:;
    }
    return 0;
}

static void material_destroy(struct DemoShell* shell, void* d) {
    struct MaterialData* md = d;
    unsigned int i, n;
    if (!d) return;

    {
        struct Variable* vars = (void*)(md + 1);
        n = material_num_variables(md->type);
        for (i = 0; i < n; i++) {
            variable_free(vars++);
        }
        free(md->material->params);
    }
    free(md->material);
    free(d);
}

static void* params_copy(const void* params, size_t size) {
    void* r;
    if ((r = malloc(size))) {
        memcpy(r, params, size);
    }
    return r;
}

static struct Material* create_material(struct DemoShell* shell, enum MaterialType type, const void* params, enum MeshFlags mflags) {
    struct Material* m = NULL;
    if (!pthread_mutex_lock(&shell->swapMutex)) {
        viewer_make_current(shell->viewer);
        switch (type) {
            case SOLID: m = (((const struct SolidMaterialParams2*)params)->overlay ? solid_overlay_material_new : solid_material_new)(mflags, params); break;
            case PHONG: m = phong_material_new(mflags, params); break;
            case PBR:   m = pbr_material_new(mflags, params); break;
            default:;
        }
        viewer_make_current(NULL);
        pthread_mutex_unlock(&shell->swapMutex);
    }
    return m;
}

static struct ShellRefCount* shell_new_material(struct DemoShell* shell, enum MaterialType type, const void* params, unsigned int requiredFlags) {
    struct ShellRefCount *r;
    struct MaterialData* m = NULL;
    struct Variable* vars;
    unsigned int i, numVars = material_num_variables(type);

    if (!(r = shell_new_refcount(shell, material_destroy, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return NULL;
    }
    if (!(m = malloc(sizeof(*m) + numVars * sizeof(*vars)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        return NULL;
    }
    if (!(m->material = create_material(shell, type, params, requiredFlags))) {
        fprintf(stderr, "Error: failed to create material\n");
        shell_decref(r);
        free(m);
        return NULL;
    }
    r->data = m;
    m->type = type;
    switch (type) {
        case SOLID: m->material->params = params_copy(params, sizeof(struct SolidMaterialParams)); break;
        case PHONG: m->material->params = params_copy(params, sizeof(struct PhongMaterialParams)); break;
        case PBR:   m->material->params = params_copy(params, sizeof(struct PBRMaterialParams)); break;
        default:    m->material->params = NULL;
    }
    m->requiredFlags = requiredFlags;
    vars = (void*)(m + 1);
    for (i = 0; i < numVars; i++) {
        (*vars++).type = VOID;
    }
    return r;
}

static int mat_program(const void* src, struct Variable* dest) {
    struct MaterialData* md = ((const struct ShellRefCount*)src)->data;
    struct Material* m = md->material;
    variable_make_int(m->program, dest);
    return 1;
}

static int mat_mode_get(const void* src, struct Variable* dest) {
    const struct MaterialData* md = ((const struct ShellRefCount*)src)->data;
    const struct Material* m = md->material;
    switch (m->polygonMode) {
        case GL_POINT: return variable_make_string("point", 5, dest);
        case GL_LINE:  return variable_make_string("line", 4, dest);
        case GL_FILL:  return variable_make_string("fill", 4, dest);
        default:       return variable_make_string("invalid", 7, dest);
    }
    return 0;
}

static int mat_mode_set(const struct Variable* src, void* dest) {
    char* s;
    struct MaterialData* md = ((const struct ShellRefCount*)dest)->data;
    struct Material* m = md->material;
    int ret = 1;
    if ((s = variable_to_string(src))) {
        if (!strcmp(s, "point")) {
            m->polygonMode = GL_POINT;
        } else if (!strcmp(s, "line")) {
            m->polygonMode = GL_LINE;
        } else if (!strcmp(s, "fill")) {
            m->polygonMode = GL_FILL;
        } else {
            ret = 0;
        }
        free(s);
    } else {
        ret = 0;
    }
    return ret;
}

int mat_refptr_copy(const struct Variable* src, struct Variable* dest) {
    struct ShellRefCount* r = src->data.dpointer.sdata;
    if (!shell_incref(r)) return 0;
    variable_make_ptr(src->data.dpointer.info, src->data.dpointer.gdata, src->data.dpointer.sdata, dest);
    return 1;
}

void mat_refptr_free(struct Variable* var) {
    shell_decref(var->data.dpointer.sdata);
}

static const struct PointerInfo matModePtr = {mat_mode_get, mat_mode_set, mat_refptr_copy, mat_refptr_free};

static int mat_mode(const void* src, struct Variable* dest) {
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&matModePtr, (void*)src, (void*)src, dest);
    return 1;
}

static int mat_copy(const struct Variable* src, struct Variable* dest) {
    struct ShellRefCount* r = src->data.dstruct.data;
    if (!shell_incref(r)) return 0;
    variable_make_struct(src->data.dstruct.info, r, dest);
    return 1;
}

static void mat_free(void* data) {
    shell_decref(data);
}

static int mat_var_get(const void* src, struct Variable* dest) {
    return variable_copy(src, dest);
}

static int mat_flt_set(const struct Variable* src, struct Variable* vdest, struct MatParamFloat* pdest) {
    struct Variable tmp;
    if (pdest->mode == MAT_PARAM_TEXTURED) {
        if (!variable_copy(src, &tmp)) return 0;
        if (!variable_is_texture(&tmp)) {
            variable_free(&tmp);
            return 0;
        }
        material_param_set_float_texture(pdest, variable_to_texture(&tmp)->tex);
    } else {
        if (!variable_to_float(src, &tmp.data.dfloat)) {
            return 0;
        }
        material_param_set_float_constant(pdest, tmp.data.dfloat);
        tmp.type = FLOAT;
    }
    variable_free(vdest);
    *vdest = tmp;
    return 1;
}

static int mat_vec3_set(const struct Variable* src, struct Variable* vdest, struct MatParamVec3* pdest) {
    struct Variable tmp;
    if (pdest->mode == MAT_PARAM_TEXTURED) {
        if (!variable_copy(src, &tmp)) return 0;
        if (!variable_is_texture(&tmp)) {
            variable_free(&tmp);
            return 0;
        }
        material_param_set_vec3_texture(pdest, variable_to_texture(&tmp)->tex);
    } else if (variable_is_multi(src)) {
        material_param_set_vec3_shared(pdest, src->data.dpointer.gdata);
        tmp = *src;
    } else {
        if (!variable_to_vec3(src, tmp.data.dvec3)) {
            return 0;
        }
        material_param_set_vec3_constant(pdest, tmp.data.dvec3);
        tmp.type = VEC3;
    }
    variable_free(vdest);
    *vdest = tmp;
    return 1;
}

static int mat_tex_set(const struct Variable* src, struct Variable* vdest, GLuint* pdest) {
    struct Variable tmp;
    if (!variable_copy(src, &tmp)) return 0;
    if (!variable_is_texture(&tmp)) {
        variable_free(&tmp);
        return 0;
    }
    *pdest = variable_to_texture(&tmp)->tex;
    variable_free(vdest);
    *vdest = tmp;
    return 1;
}

#define MAT_MEMBER(mat, type, name, n) \
static int mat##_##name##_set(const struct Variable* src, void* dest) { \
    struct MaterialData* md = ((const struct ShellRefCount*)dest)->data; \
    struct Variable* v = (void*)(md + 1); \
    struct mat##MaterialParams* m = md->material->params; \
    return mat_##type##_set(src, v + n, &m->name); \
} \
static const struct PointerInfo mat##name##Ptr = {mat_var_get, mat##_##name##_set, mat_refptr_copy, mat_refptr_free}; \
static int mat##_##name(const void* src, struct Variable* dest) { \
    struct MaterialData* md = ((const struct ShellRefCount*)src)->data; \
    struct Variable* v = (void*)(md + 1); \
    if (!shell_incref((struct ShellRefCount*)src)) return 0; \
    variable_make_ptr(&mat##name##Ptr, v + n, (void*)src, dest); \
    return 1; \
}

static int parse_color_param(const struct Variable* arg, struct Variable* vDest, struct MatParamVec3* param) {
    if (variable_is_multi(arg)) {
        material_param_set_vec3_shared(param, arg->data.dpointer.gdata);
        *vDest = *arg;
    } else if (variable_to_vec3(arg, param->value.constant)) {
        param->mode = MAT_PARAM_CONSTANT;
        vDest->type = VEC3;
        memcpy(vDest->data.dvec3, param->value.constant, sizeof(vDest->data.dvec3));
    } else {
        if (!variable_copy_dereference(arg, vDest)) {
            fprintf(stderr, "Error: failed to copy argument\n");
            return 0;
        }
        if (!variable_is_texture(vDest)) {
            fprintf(stderr, "Error: invalid argument\n");
            variable_free(vDest);
            return 0;
        }
        material_param_set_vec3_texture(param, variable_to_texture(vDest)->tex);
    }
    return 1;
}

static int parse_float_param(const struct Variable* arg, struct Variable* vDest, struct MatParamFloat* param) {
    if (variable_to_float(arg, &param->value.constant)) {
        param->mode = MAT_PARAM_CONSTANT;
        variable_make_float(param->value.constant, vDest);
    } else {
        if (!variable_copy_dereference(arg, vDest)) {
            fprintf(stderr, "Error: failed to copy argument\n");
            return 0;
        }
        if (!variable_is_texture(vDest)) {
            fprintf(stderr, "Error: invalid argument\n");
            variable_free(vDest);
            return 0;
        }
        material_param_set_float_texture(param, variable_to_texture(vDest)->tex);
    }
    return 1;
}

static int parse_tex_param(const struct Variable* arg, struct Variable* vDest, GLuint* tex) {
    if (!variable_copy_dereference(arg, vDest)) {
        fprintf(stderr, "Error: failed to copy argument\n");
        return 0;
    }
    if (!variable_is_texture(vDest)) {
        fprintf(stderr, "Error: invalid argument\n");
        variable_free(vDest);
        return 0;
    }
    *tex = variable_to_texture(vDest)->tex;
    return 1;
}

MAT_MEMBER(Solid, vec3, color, 0)

static const struct FieldInfo structSolidMatFields[] = {
    {"color", Solid_color},
    {"program", mat_program},
    {"mode", mat_mode},
    {0}
};

static const struct StructInfo structSolidMat = {"SolidMat", structSolidMatFields, mat_copy, mat_free};

static unsigned int solid_required_flags(const struct SolidMaterialParams* params) {
    return (params->color.mode == MAT_PARAM_TEXTURED || (params->alpha.mode != ALPHA_DISABLED && params->alpha.alpha.mode == MAT_PARAM_TEXTURED)) ? MESH_TEXCOORDS : 0;
}

static int solid(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable vColor, *vars;
    struct DemoShell* shell = data;
    struct ShellRefCount *r;
    struct SolidMaterialParams2 params;

    solid_material_params_init(&params.solid);
    params.overlay = 0;
    if (nArgs == 2) {
        long b;
        if (!variable_to_int(args + 1, &b)) {
            fprintf(stderr, "Error: solid() expected integer 2nd argument\n");
            return 0;
        }
        if (b) params.overlay = 1;
        nArgs--;
    }
    if (nArgs == 1) {
        if (!parse_color_param(args, &vColor, &params.solid.color)) return 0;
    } else {
        fprintf(stderr, "Error: solid() takes one argument\n");
        return 0;
    }
    if (!(r = shell_new_material(shell, SOLID, &params, solid_required_flags(&params.solid)))) {
        variable_free(&vColor);
        return 0;
    }
    vars = (void*)(((struct MaterialData*)r->data) + 1);
    vars[0] = vColor;
    variable_make_struct(&structSolidMat, r, ret);
    return 1;
}

MAT_MEMBER(Phong, vec3, ambient, 0)
MAT_MEMBER(Phong, vec3, diffuse, 1)
MAT_MEMBER(Phong, vec3, specular, 2)
MAT_MEMBER(Phong, flt, shininess, 3)
MAT_MEMBER(Phong, tex, normalMap, 4)

static const struct FieldInfo structPhongMatFields[] = {
    {"ambient", Phong_ambient},
    {"diffuse", Phong_diffuse},
    {"specular", Phong_specular},
    {"shininess", Phong_shininess},
    {"normalMap", Phong_normalMap},
    {"program", mat_program},
    {"mode", mat_mode},
    {0}
};

static const struct StructInfo structPhongMat = {"PhongMat", structPhongMatFields, mat_copy, mat_free};

static unsigned int phong_required_flags(const struct PhongMaterialParams* params) {
    unsigned int requiredFlags = MESH_NORMALS;
    if (params->ambient.mode == MAT_PARAM_TEXTURED || params->diffuse.mode == MAT_PARAM_TEXTURED || params->specular.mode == MAT_PARAM_TEXTURED || params->shininess.mode == MAT_PARAM_TEXTURED || (params->alpha.mode != ALPHA_DISABLED && params->alpha.alpha.mode == MAT_PARAM_TEXTURED)) requiredFlags |= MESH_TEXCOORDS;
    if (params->normalMap) requiredFlags |= MESH_TEXCOORDS | MESH_TANGENTS;
    return requiredFlags;
}

static int phong(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable vAmbient, vDiffuse, vSpecular, vShininess, vNormalMap, *vars;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;
    struct PhongMaterialParams params;

    phong_material_params_init(&params);
    if (nArgs < 1 || nArgs > 5) {
        fprintf(stderr, "Error: expected 1 to 5 arguments for phong()\n");
        return 0;
    }
    if (nArgs < 3) {
        material_param_set_vec3_elems(&params.ambient, 0, 0, 0);
        vAmbient.type = VEC3;
        zero3v(vAmbient.data.dvec3);
    } else {
        if (!parse_color_param(args++, &vAmbient, &params.ambient)) return 0;
    }
    if (!parse_color_param(args++, &vDiffuse, &params.diffuse)) {
        variable_free(&vAmbient);
        return 0;
    }
    if (nArgs < 2) {
        material_param_set_vec3_elems(&params.specular, 0, 0, 0);
        vSpecular.type = VEC3;
        zero3v(vSpecular.data.dvec3);
    } else if (!parse_color_param(args++, &vSpecular, &params.specular)) {
        variable_free(&vAmbient);
        variable_free(&vDiffuse);
        return 0;
    }
    if (nArgs < 4) {
        material_param_set_float_constant(&params.shininess, 1);
        variable_make_float(1, &vShininess);
    } else if (!parse_float_param(args++, &vShininess, &params.shininess)) {
        variable_free(&vAmbient);
        variable_free(&vDiffuse);
        variable_free(&vSpecular);
        return 0;
    }
    if (nArgs < 5) {
        vNormalMap.type = VOID;
        params.normalMap = 0;
    } else if (!parse_tex_param(args++, &vNormalMap, &params.normalMap)) {
        variable_free(&vAmbient);
        variable_free(&vDiffuse);
        variable_free(&vSpecular);
        variable_free(&vShininess);
        return 0;
    }
    if (!(r = shell_new_material(shell, PHONG, &params, phong_required_flags(&params)))) {
        variable_free(&vAmbient);
        variable_free(&vDiffuse);
        variable_free(&vSpecular);
        variable_free(&vShininess);
        variable_free(&vNormalMap);
        return 0;
    }
    vars = (void*)(((struct MaterialData*)r->data) + 1);
    vars[0] = vAmbient;
    vars[1] = vDiffuse;
    vars[2] = vSpecular;
    vars[3] = vShininess;
    vars[4] = vNormalMap;
    variable_make_struct(&structPhongMat, r, ret);
    return 1;
}

MAT_MEMBER(PBR, vec3, albedo, 0)
MAT_MEMBER(PBR, flt, metalness, 1)
MAT_MEMBER(PBR, flt, roughness, 2)
MAT_MEMBER(PBR, tex, normalMap, 3)

static const struct FieldInfo structPBRMatFields[] = {
    {"albedo", PBR_albedo},
    {"metalness", PBR_metalness},
    {"roughness", PBR_roughness},
    {"normalMap", PBR_normalMap},
    {"program", mat_program},
    {"mode", mat_mode},
    {0}
};

static const struct StructInfo structPBRMat = {"PBRMat", structPBRMatFields, mat_copy, mat_free};

static unsigned int pbr_required_flags(const struct PBRMaterialParams* params) {
    unsigned int requiredFlags = MESH_NORMALS;
    if (params->albedo.mode == MAT_PARAM_TEXTURED || params->metalness.mode == MAT_PARAM_TEXTURED || params->roughness.mode == MAT_PARAM_TEXTURED || (params->alpha.mode != ALPHA_DISABLED && params->alpha.alpha.mode == MAT_PARAM_TEXTURED)) requiredFlags |= MESH_TEXCOORDS;
    if (params->normalMap) requiredFlags |= MESH_TEXCOORDS | MESH_TANGENTS;
    return requiredFlags;
}

static int pbr(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable vAlbedo, vMetalness, vRoughness, vNormalMap, *vars;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;
    struct PBRMaterialParams params;

    pbr_material_params_init(&params);
    params.ibl = &shell->ibl;
    if (nArgs < 3 || nArgs > 4) {
        fprintf(stderr, "Error: expected 3 or 4 arguments for pbr()\n");
        return 0;
    }
    if (!parse_color_param(args++, &vAlbedo, &params.albedo)) return 0;
    if (!parse_float_param(args++, &vMetalness, &params.metalness)) {
        variable_free(&vAlbedo);
        return 0;
    }
    if (!parse_float_param(args++, &vRoughness, &params.roughness)) {
        variable_free(&vAlbedo);
        variable_free(&vMetalness);
        return 0;
    }
    if (nArgs < 4) {
        vNormalMap.type = VOID;
        params.normalMap = 0;
    } else if (!parse_tex_param(args++, &vNormalMap, &params.normalMap)) {
        variable_free(&vAlbedo);
        variable_free(&vMetalness);
        variable_free(&vRoughness);
        return 0;
    }
    if (!(r = shell_new_material(shell, PBR, &params, pbr_required_flags(&params)))) {
        variable_free(&vAlbedo);
        variable_free(&vMetalness);
        variable_free(&vRoughness);
        variable_free(&vNormalMap);
        return 0;
    }
    vars = (void*)(((struct MaterialData*)r->data) + 1);
    vars[0] = vAlbedo;
    vars[1] = vMetalness;
    vars[2] = vRoughness;
    vars[3] = vNormalMap;
    variable_make_struct(&structPBRMat, r, ret);
    return 1;
}

int shell_make_material_vars(struct DemoShell* shell) {
    return builtin_function_with_data(shell, "solid", solid, shell)
        && builtin_function_with_data(shell, "phong", phong, shell)
        && builtin_function_with_data(shell, "pbr", pbr, shell);
}

int variable_is_material(const struct Variable* var) {
    return var->type == STRUCT
        && (var->data.dstruct.info == &structSolidMat
         || var->data.dstruct.info == &structPhongMat
         || var->data.dstruct.info == &structPBRMat);
}

struct Material* variable_to_material(const struct Variable* var) {
    if (variable_is_material(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct MaterialData* m = r->data;
        return m->material;
    }
    return NULL;
}

struct MaterialData* variable_to_material_data(const struct Variable* var) {
    if (variable_is_material(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        return r->data;
    }
    return NULL;
}

static int import_mat_vec3_param(struct DemoShell* shell, const struct MatParamVec3* param, struct Variable* dest) {
    switch (param->mode) {
        case MAT_PARAM_CONSTANT:
            memcpy(dest->data.dvec3, param->value.constant, sizeof(Vec3));
            dest->type = VEC3;
            return 1;
        case MAT_PARAM_SHARED:
            memcpy(dest->data.dvec3, param->value.shared, sizeof(Vec3));
            dest->type = VEC3;
            return 1;
        case MAT_PARAM_TEXTURED:
            return variable_make_texture(shell, param->value.texture, "imported texture", dest);
    }
    return 0;
}

static int import_mat_float_param(struct DemoShell* shell, const struct MatParamFloat* param, struct Variable* dest) {
    switch (param->mode) {
        case MAT_PARAM_CONSTANT:
            variable_make_float(param->value.constant, dest);
            return 1;
        case MAT_PARAM_SHARED:
            variable_make_float(*param->value.shared, dest);
            return 1;
        case MAT_PARAM_TEXTURED:
            return variable_make_texture(shell, param->value.texture, "imported texture", dest);
    }
    return 0;
}

static const struct StructInfo* material_get_struct(enum MaterialType type) {
    switch (type) {
        case SOLID: return &structSolidMat;
        case PHONG: return &structPhongMat;
        case PBR: return &structPBRMat;
        default:;
    }
    return NULL;
}

static enum MaterialType material_type(const struct Material* material) {
    if (material_is_solid(material)) return SOLID;
    if (material_is_phong(material)) return PHONG;
    if (material_is_pbr(material)) return PBR;
    return NOT_SUPPORTED_MAT;
}

int variable_make_material(struct DemoShell* shell, struct Material* material, struct Variable* dest) {
    static const size_t psize[] = {sizeof(struct SolidMaterialParams), sizeof(struct PhongMaterialParams), sizeof(struct PBRMaterialParams)};
    struct ShellRefCount* r;
    struct MaterialData* m;
    struct Variable* vars;
    enum MaterialType type;
    unsigned int i, numVars;
    int ok = 1;

    for (i = 0; i < shell->numRefcounts; i++) {
        if (shell->refcount[i] && shell->refcount[i]->destroy == material_destroy && (m = shell->refcount[i]->data)
         && m->material->load == material->load
         && m->material->polygonMode == material->polygonMode
         && m->material->program == material->program
         && m->type >= 0 && m->type < (sizeof(psize) / sizeof(*psize)) && !memcmp(m->material->params, material->params, psize[m->type])) {
            const struct StructInfo* sinfo;
            if (!(sinfo = material_get_struct(m->type)) || !shell_incref(shell->refcount[i])) {
                return 0;
            }
            variable_make_struct(sinfo, shell->refcount[i], dest);
            return 1;
        }
    }
    type = material_type(material);
    numVars = material_num_variables(type);
    if (!(r = shell_new_refcount(shell, material_destroy, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return 0;
    }
    if (!(m = malloc(sizeof(*m) + numVars * sizeof(*vars)))
     || !(m->material = malloc(sizeof(*m->material)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        free(m);
        return 0;
    }
    r->data = m;
    *m->material = *material;
    m->type = type;
    vars = (void*)(m + 1);
    for (i = 0; i < numVars; i++) {
        (*vars++).type = VOID;
    }
    vars = (void*)(m + 1);

    switch (type) {
        case SOLID:
            m->requiredFlags = solid_required_flags(material->params);
            if (!(m->material->params = params_copy(material->params, sizeof(struct SolidMaterialParams)))
             || !import_mat_vec3_param(shell, &((const struct SolidMaterialParams*)material->params)->color, vars)) {
                ok = 0;
            }
            break;
        case PHONG:
            m->requiredFlags = phong_required_flags(material->params);
            if (!(m->material->params = params_copy(material->params, sizeof(struct PhongMaterialParams)))
             || !import_mat_vec3_param(shell, &((const struct PhongMaterialParams*)material->params)->ambient, vars)
             || !import_mat_vec3_param(shell, &((const struct PhongMaterialParams*)material->params)->diffuse, vars + 1)
             || !import_mat_vec3_param(shell, &((const struct PhongMaterialParams*)material->params)->specular, vars + 2)
             || !import_mat_float_param(shell, &((const struct PhongMaterialParams*)material->params)->shininess, vars + 3)
             || (((const struct PhongMaterialParams*)material->params)->normalMap && !variable_make_texture(shell, ((const struct PhongMaterialParams*)material->params)->normalMap, "imported texture", vars + 4))) {
                ok = 0;
            }
            break;
        case PBR:
            m->requiredFlags = pbr_required_flags(material->params);
            if (!(m->material->params = params_copy(material->params, sizeof(struct PBRMaterialParams)))
             || !import_mat_vec3_param(shell, &((const struct PBRMaterialParams*)material->params)->albedo, vars)
             || !import_mat_float_param(shell, &((const struct PBRMaterialParams*)material->params)->metalness, vars + 1)
             || !import_mat_float_param(shell, &((const struct PBRMaterialParams*)material->params)->roughness, vars + 2)
             || (((const struct PBRMaterialParams*)material->params)->normalMap && !variable_make_texture(shell, ((const struct PBRMaterialParams*)material->params)->normalMap, "imported texture", vars + 3))) {
                ok = 0;
            }
            break;
        default:
            ok = 0;
    }
    if (ok) {
        variable_make_struct(material_get_struct(type), r, dest);
    } else {
        struct TextureData* td;
        for (i = 0; i < numVars; i++) {
            if ((td = variable_to_texture(vars + i))) td->tex = 0;
            variable_free(vars + i);
            vars[i].type = VOID;
        }
        shell_decref(r);
    }
    return ok;
}
