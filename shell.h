#include <pthread.h>
#include <3dmr/scene/scene.h>
#include "variable.h"
#include "colors.h"

#ifndef TDSH_SHELL_H
#define TDSH_SHELL_H

struct ShellVarNode {
    union ShellVarNodePtr {
        struct ShellVarNode* children;
        struct Variable* var;
    } value;
    unsigned int numChildren;
    char c;
};

struct ShellConfig {
    unsigned int defWidth, defHeight;
    const char* execCmd;
    char** argv;
    unsigned int argc;
};

struct DemoShell {
    struct Scene scene;
    struct AmbientLight ambientLight;
    unsigned int numDirectionalLights, numPointLights, numSpotLights;
    struct IBL ibl;
    struct ShellRefCount *rdlight[MAX_DIRECTIONAL_LIGHTS], *rplight[MAX_POINT_LIGHTS], *rslight[MAX_SPOT_LIGHTS];
    struct Viewer* viewer;
    struct Skybox* skybox;
    struct ShellRefCount** refcount;
    unsigned int numRefcounts;
    struct ShellVarNode* contexts;
    unsigned int numContexts;
    struct ShellConfig config;
    float fps;
    int running; /* mainMutex */
    int interactive; /* inputMutex */
    int cameraChanged, lightsChanged, graphChanged;
    pthread_mutex_t mainMutex, inputMutex, swapMutex;
    pthread_cond_t mainCond, inputCond, swapCond;
    char *ps1, *ps2;
    const char *prompt, *curFile, *curLine; /* inputMutex */
    unsigned int curLineNo; /* inputMutex */
    struct ShellColors colors;
    struct Variable activeCamera, mouseCb, scrollCb, keyCb, resizeCb;
    double dt; /* swapMutex */
    int renderDone; /* swapMutex */
    int enableRender, enableExec;
    unsigned int lasterr;
};

struct ShellRefCount {
    void (*destroy)(struct DemoShell*, void*);
    struct DemoShell* shell;
    void* data;
    unsigned int refcount;
};

void shell_config_default(struct ShellConfig* config);

int shell_init(struct DemoShell* shell, const struct ShellConfig* config);
void shell_free(struct DemoShell* shell);

struct ShellVarNode* shell_active_context(struct DemoShell* shell);
struct ShellVarNode* shell_push_context(struct DemoShell* shell);
void shell_pop_context(struct DemoShell* shell);

struct Variable* shell_add_variable(struct DemoShell* shell, const char* name);
struct Variable* shell_get_variable(struct DemoShell* shell, const char* name, int recursive);
void shell_del_variable(struct DemoShell* shell, const char* name);
void shell_free_variables(struct DemoShell* shell);

struct ShellRefCount* shell_new_refcount(struct DemoShell* shell, void (*destroy)(struct DemoShell*, void*), void* data);
int shell_incref(struct ShellRefCount* r);
void shell_decref(struct ShellRefCount* r);

#endif
