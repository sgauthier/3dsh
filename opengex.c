#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3dmr/scene/import.h>
#include <3dmr/scene/opengex.h>
#include <3dmr/img/png.h>
#ifdef TDMR_OPENGEX
#include <liboddl/liboddl.h>
#endif
#include "builtins.h"
#include "node.h"
#include "render.h"
#include "camera.h"
#include "dlight.h"
#include "plight.h"
#include "material.h"
#include "mesh.h"
#include "texture.h"
#include "vertex_array.h"

#ifdef TDMR_OPENGEX
static void ogex_free_node(struct Node* node) {
    if (node->type == NODE_GEOMETRY) free(node->data.geometry);
    free(node);
}
#endif

static int import_ogex(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
#ifndef TDMR_OPENGEX
    fprintf(stderr, "Error: opengex support was not compiled\n");
    return 0;
#else
    struct Node* root;
    struct ImportSharedData shared;
    struct DemoShell* shell = data;
    FILE* f;
    char *path, *sep;
    int ok = 0;

    if (nArgs != 1 || !(path = variable_to_string(args))) {
        fprintf(stderr, "Error: ogex_load(): expected path argument\n");
        return 0;
    }
    if (!(f = fopen(path, "r"))) {
        fprintf(stderr, "Error: ogex_load(): cannot open '%s'\n", path);
        free(path);
        return 0;
    }
    if (!(root = malloc(sizeof(*root)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        free(path);
        fclose(f);
        return 0;
    }
    node_init(root);
    if (pthread_mutex_lock(&shell->swapMutex)) {
        fprintf(stderr, "Error: failed to lock mutex\n");
    } else {
        if ((sep = strrchr(path, '/'))) {
            *sep = 0;
        } else {
            free(path);
            path = NULL;
        }
        viewer_make_current(shell->viewer);
        import_init_shared_data(&shared);
        if (!ogex_load(root, f, path, &shared, NULL)) {
            fprintf(stderr, "Error: failed to import opengex scene\n");
        } else if (!variable_make_node(shell, root, ret)) {
            fprintf(stderr, "Error: failed to import opengex scene in shell format\n");
            import_free_shared_data(&shared);
        } else {
            unsigned int i;
            ok = 1;
            /* we don't use import_free_shared_data here, as we steal OpenGL refs */
            for (i = 0; i < shared.numVA; i++) free(shared.va[i]);
            for (i = 0; i < shared.numMatParams; i++) free(shared.matParams[i]);
            for (i = 0; i < shared.numSkins; i++) free(shared.skins[i]); /* TODO: skin_del when not in use anymore, this is currently leaked */
            free(shared.matParams);
            free(shared.va);
            free(shared.skins);
        }
        viewer_make_current(NULL);
        pthread_mutex_unlock(&shell->swapMutex);
    }
    free(path);
    fclose(f);
    nodes_free(root, ogex_free_node);
    return ok;
#endif
}

#ifdef TDMR_OPENGEX
struct Ref {
    unsigned int n;
    const void* var;
};

static int add_ref(struct Ref** refs, unsigned int* num, unsigned int n, const void* var) {
    struct Ref* tmp;
    if (!(tmp = realloc(*refs, (*num + 1) * sizeof(**refs)))) return 0;
    *refs = tmp;
    tmp += (*num)++;
    tmp->n = n;
    tmp->var = var;
    return 1;
}

static const struct Ref* get_ref(const struct Ref* refs, unsigned int num, const void* var) {
    while (num) {
        if (refs->var == var) {
            return refs;
        }
        num--;
        refs++;
    }
    return NULL;
}

struct Stack {
    union SrcVar {
        const struct Variable* varptr;
        struct Variable var;
    } src;
    int ptr;
    struct ODDLStructure* parent;
};

static int push(struct Stack** stack, unsigned int* num, const struct Variable* var, int ptr, struct ODDLStructure* parent) {
    struct Stack* tmp;
    if (!(tmp = realloc(*stack, (*num + 1) * sizeof(**stack)))) return 0;
    *stack = tmp;
    tmp += (*num)++;
    if (ptr) {
        tmp->src.varptr = var;
    } else {
        tmp->src.var = *var;
    }
    tmp->ptr = ptr;
    tmp->parent = parent;
    return 1;
}

static int structure_name(struct ODDLStructure* s, const char* prefix, unsigned int n) {
    size_t l = strlen(prefix);
    if (l >= ((size_t)-32) || !(s->name.str = malloc(l + 32))) return 0;
    sprintf(s->name.str, "%s%u", prefix, n);
    s->name.globalNameIdx = 0;
    return 1;
}

static int structure_identifier(struct ODDLStructure* s, const char* identifier) {
    size_t l = strlen(identifier);
    if (l >= ((size_t)-1) || !(s->identifier = malloc(++l))) return 0;
    memcpy(s->identifier, identifier, l);
    return 1;
}

static int ogex_set_metrics(struct ODDLDoc* doc) {
    struct ODDLStructure *smetric, *sdata;
    char* value;
    if (!(smetric = oddl_new_structure()) || !oddl_structure_add_child(doc->root, smetric)) {
        free(smetric);
    } else if (!(sdata = oddl_new_data_structure(TYPE_STRING, 1, 1)) || !oddl_structure_add_child(smetric, sdata)) {
        free(sdata);
    } else if (!structure_identifier(smetric, "Metric") || !oddl_structure_add_string_property(smetric, "key", "up") || !(value = malloc(2))) {
    } else {
        strcpy(value, "y");
        *(char**)(sdata->dataList) = value;
        return 1;
    }
    return 0;
}

static int variables_to_ogex(struct DemoShell* shell, const struct Variable* vars, unsigned int nVars, struct ODDLDoc* doc, const char* respath) {
    struct Stack* stack = NULL;
    struct Ref* refs = NULL;
    unsigned int n = 0, numRefs = 0, numNodes = 0, numLights = 0, numCameras = 0, numGeoms = 0, numMats = 0, numTextures = 0;
    int ok = 1;

    while (nVars && ok) {
        ok = push(&stack, &n, vars + --nVars, 1, doc->root);
    }
    while (n && ok) {
        struct Variable tmp;
        union DataPtr {
            struct Node* node;
            const struct DirectionalLight* dlight;
            const struct PointLight* plight;
            const struct Projection* camera;
            const struct VertexArray* vertexArray;
            const struct Mesh* mesh;
            const struct MaterialData* material;
            const struct TextureData* texture;
        } dptr;
        union Type {
            int scene, dlight, vertexArray;
        } is;
        const struct Ref* ref;
        const struct Variable* srcVar = stack[n - 1].ptr ? stack[n - 1].src.varptr : ((const struct Variable*)&stack[n - 1].src.var);
        int vptr = variable_move_dereference(srcVar, &tmp);
        if ((is.scene = ((dptr.node = variable_to_scene(&tmp)) != NULL)) || (dptr.node = variable_to_node(&tmp))) {
            char refname[64];
            unsigned int i, matref = 0;
            struct ODDLStructure *snode, *subs;
            if (is.scene) {
                ok = (dptr.node->type == NODE_EMPTY);
                refname[0] = 0;
            } else {
                struct NodeVarData* ndata = node_get_var_data(dptr.node);
                switch (dptr.node->type) {
                    case NODE_EMPTY:
                        refname[0] = 0;
                        break;
                    case NODE_DLIGHT:
                    case NODE_PLIGHT:
                        if (!(ref = get_ref(refs, numRefs, (dptr.node->type == NODE_DLIGHT) ? ((void*)(variable_to_dlight(&ndata->data.light))) : ((void*)(variable_to_plight(&ndata->data.light)))))) {
                            ok = push(&stack, &n, &ndata->data.light, 1, doc->root);
                            goto next;
                        }
                        sprintf(refname, "$light%u", ref->n);
                        break;
                    case NODE_CAMERA:
                        if (!(ref = get_ref(refs, numRefs, variable_to_camera_proj(&ndata->data.camera)))) {
                            ok = push(&stack, &n, &ndata->data.camera, 1, doc->root);
                            goto next;
                        }
                        sprintf(refname, "$camera%u", ref->n);
                        break;
                    case NODE_GEOMETRY:
                        if (!(ref = get_ref(refs, numRefs, variable_to_material_data(&ndata->data.geom.material)))) {
                            ok = push(&stack, &n, &ndata->data.geom.material, 1, doc->root);
                            goto next;
                        }
                        matref = ref->n;
                        if (!(ref = get_ref(refs, numRefs, variable_to_vertex_array(&ndata->data.geom.vertexArray)))) {
                            ok = push(&stack, &n, &ndata->data.geom.vertexArray, 1, doc->root);
                            goto next;
                        }
                        sprintf(refname, "$geometry%u", ref->n);
                        break;
                    default:
                        ok = 0;
                }
            }
            if (!ok) goto next;
            node_update_matrices(dptr.node);
            if (!(snode = oddl_new_structure()) || !oddl_structure_add_child(stack[n - 1].parent, snode)) {
                free(snode);
                ok = 0;
            } else {
                --n;
                structure_name(snode, "node", ++numNodes); /* name is optional, so we don't care if this fails */
                switch (dptr.node->type) {
                    case NODE_EMPTY:    ok = structure_identifier(snode, "Node"); break;
                    case NODE_GEOMETRY: ok = structure_identifier(snode, "GeometryNode"); break;
                    case NODE_CAMERA:   ok = structure_identifier(snode, "CameraNode"); break;
                    case NODE_DLIGHT:   ok = structure_identifier(snode, "LightNode"); break;
                    case NODE_PLIGHT:   ok = structure_identifier(snode, "LightNode"); break;
                    default:            ok = 0; break;
                }
                if (ok) {
                    if (!(subs = oddl_new_structure()) || !oddl_structure_add_child(snode, subs)) {
                        free(subs);
                        ok = 0;
                    } else {
                        struct ODDLStructure* t;
                        if ((ok = structure_identifier(subs, "Transform"))) {
                            if (!(t = oddl_new_data_structure(TYPE_FLOAT32, 1, 16)) || !oddl_structure_add_child(subs, t)) {
                                free(t);
                                ok = 0;
                            } else {
                                memcpy(t->dataList, dptr.node->transform, sizeof(Mat4));
                            }
                        }
                    }
                    if (ok && refname[0]) {
                        if (!(subs = oddl_new_structure()) || !oddl_structure_add_child(snode, subs)) {
                            free(subs);
                            ok = 0;
                        } else {
                            struct ODDLStructure* r;
                            if ((ok = structure_identifier(subs, "ObjectRef"))) {
                                if (!(r = oddl_new_data_structure(TYPE_REF, 1, 1)) || !oddl_structure_add_child(subs, r)) {
                                    free(r);
                                    ok = 0;
                                } else {
                                    struct ODDLRef* ref = r->dataList;
                                    if ((ok = (ref->refStr = malloc(strlen(refname) + 1)) != NULL)) {
                                        strcpy(ref->refStr, refname);
                                        ref->ref = NULL;
                                    }
                                }
                            }
                        }
                    }
                    if (ok && matref) {
                        if (!(subs = oddl_new_structure()) || !oddl_structure_add_child(snode, subs)) {
                            free(subs);
                            ok = 0;
                        } else {
                            struct ODDLStructure* r;
                            if ((ok = structure_identifier(subs, "MaterialRef"))) {
                                if (!(r = oddl_new_data_structure(TYPE_REF, 1, 1)) || !oddl_structure_add_child(subs, r)) {
                                    free(r);
                                    ok = 0;
                                } else {
                                    struct ODDLRef* ref = r->dataList;
                                    if ((ok = (ref->refStr = malloc(64)) != NULL)) {
                                        sprintf(ref->refStr, "$material%u", matref);
                                        ref->ref = NULL;
                                    }
                                }
                            }
                        }
                    }
                }
                for (i = 0; ok && i < dptr.node->nbChildren; i++) {
                    struct NodeVarData* nd = node_get_var_data(dptr.node->children[i]);
                    struct Variable v;
                    variable_make_struct(node_struct_type(dptr.node->children[i]->type), nd->self, &v);
                    ok = push(&stack, &n, &v, 0, snode);
                }
            }
        } else if ((is.dlight = (dptr.dlight = variable_to_dlight(&tmp)) != NULL) || (dptr.plight = variable_to_plight(&tmp))) {
            struct ODDLStructure *slight, *scolor, *sdata, *sparam;
            if (!(slight = oddl_new_structure()) || !oddl_structure_add_child(stack[n - 1].parent, slight)) {
                free(slight);
                ok = 0;
            } else if (!structure_name(slight, "light", ++numLights) || !structure_identifier(slight, "LightObject") || !oddl_structure_add_string_property(slight, "type", is.dlight ? "infinite" : "point")) {
                ok = 0;
            } else if (!(scolor = oddl_new_structure()) || !oddl_structure_add_child(slight, scolor)) {
                free(scolor);
                ok = 0;
            } else if (!structure_identifier(scolor, "Color") || !oddl_structure_add_string_property(scolor, "attrib", "light")) {
                ok = 0;
            } else if (!(sdata = oddl_new_data_structure(TYPE_FLOAT32, 1, 3)) || !oddl_structure_add_child(scolor, sdata)) {
                free(sdata);
                ok = 0;
            } else {
                memcpy(sdata->dataList, is.dlight ? dptr.dlight->color : dptr.plight->color, sizeof(Vec3));
                if (!is.dlight) {
                    if (!(scolor = oddl_new_structure()) || !oddl_structure_add_child(slight, scolor)) {
                        free(scolor);
                        ok = 0;
                    } else if (!structure_identifier(scolor, "Atten") || !oddl_structure_add_string_property(scolor, "curve", "inverse_square")) {
                        ok = 0;
                    } else if (!(sparam = oddl_new_structure()) || !oddl_structure_add_child(scolor, sparam)) {
                        free(sparam);
                        ok = 0;
                    } else if (!structure_identifier(sparam, "Param") || !oddl_structure_add_string_property(sparam, "attrib", "scale")) {
                        ok = 0;
                    } else if (!(sdata = oddl_new_data_structure(TYPE_FLOAT32, 1, 1)) || !oddl_structure_add_child(sparam, sdata)) {
                        free(sdata);
                        ok = 0;
                    } else {
                        memcpy(sdata->dataList, &dptr.plight->radius, sizeof(float));
                    }
                }
                ok = ok && add_ref(&refs, &numRefs, numLights, is.dlight ? ((void*)dptr.dlight) : ((void*)dptr.plight));
            }
            --n;
        } else if ((dptr.camera = variable_to_camera_proj(&tmp))) {
            struct ODDLStructure *scamera, *sparam, *sfov, *snear, *sfar;
            if (!(scamera = oddl_new_structure()) || !oddl_structure_add_child(stack[n - 1].parent, scamera)) {
                free(scamera);
                ok = 0;
            } else if (!structure_name(scamera, "camera", ++numCameras) || !structure_identifier(scamera, "CameraObject")) {
                ok = 0;
            } else if (!(sparam = oddl_new_structure()) || !oddl_structure_add_child(scamera, sparam)) {
                free(sparam);
                ok = 0;
            } else if (!structure_identifier(sparam, "Param") || !oddl_structure_add_string_property(sparam, "attrib", "fov")) {
                ok = 0;
            } else if (!(sfov = oddl_new_data_structure(TYPE_FLOAT32, 1, 1)) || !oddl_structure_add_child(sparam, sfov)) {
                free(sfov);
                ok = 0;
            } else if (!(sparam = oddl_new_structure()) || !oddl_structure_add_child(scamera, sparam)) {
                free(sparam);
                ok = 0;
            } else if (!structure_identifier(sparam, "Param") || !oddl_structure_add_string_property(sparam, "attrib", "near")) {
                ok = 0;
            } else if (!(snear = oddl_new_data_structure(TYPE_FLOAT32, 1, 1)) || !oddl_structure_add_child(sparam, snear)) {
                free(snear);
                ok = 0;
            } else if (!(sparam = oddl_new_structure()) || !oddl_structure_add_child(scamera, sparam)) {
                free(sparam);
                ok = 0;
            } else if (!structure_identifier(sparam, "Param") || !oddl_structure_add_string_property(sparam, "attrib", "far")) {
                ok = 0;
            } else if (!(sfar = oddl_new_data_structure(TYPE_FLOAT32, 1, 1)) || !oddl_structure_add_child(sparam, sfar)) {
                free(sfar);
                ok = 0;
            } else {
                memcpy(sfov->dataList, &dptr.camera->fov, sizeof(float));
                memcpy(snear->dataList, &dptr.camera->zNear, sizeof(float));
                memcpy(sfar->dataList, &dptr.camera->zFar, sizeof(float));
                ok = ok && add_ref(&refs, &numRefs, numCameras, dptr.camera);
            }
            --n;
        } else if ((is.vertexArray = (dptr.vertexArray = variable_to_vertex_array(&tmp)) != NULL) || (dptr.mesh = variable_to_mesh(&tmp))) {
            struct Mesh va;
            const struct Mesh* mesh;
            struct ODDLStructure *sgeom, *smesh, *sarray, *sdata;
            unsigned int i, j, k, stride, offsets[5] = {0, 0, 0, 0, 0};
            const char* attribs[5] = {"position", "normal", "texcoord", "tangent", "bitangent"};
            if (is.vertexArray) {
                if (pthread_mutex_lock(&shell->swapMutex)) {
                    ok = 0;
                } else {
                    va.numVertices = dptr.vertexArray->numVertices;
                    va.numIndices = dptr.vertexArray->numIndices;
                    viewer_make_current(shell->viewer);
                    glBindBuffer(GL_ARRAY_BUFFER, dptr.vertexArray->vbo);
                    va.vertices = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
                    if (va.numIndices) {
                        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dptr.vertexArray->ibo);
                        va.indices = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_READ_ONLY);
                    } else {
                        va.indices = NULL;
                    }
                    va.flags = dptr.vertexArray->flags;
                    mesh = &va;
                }
            } else {
                mesh = dptr.mesh;
            }
            if (!ok) goto next;
            stride = MESH_FLOATS_PER_VERTEX(mesh);
            if (mesh->flags & MESH_NORMALS) offsets[1] = 3;
            if (mesh->flags & MESH_TEXCOORDS) offsets[2] = offsets[1] + 3;
            if (mesh->flags & MESH_TANGENTS) {
                offsets[3] = (mesh->flags & MESH_TEXCOORDS) ? (offsets[2] + 2) : (offsets[1] + 3);
                offsets[4] = offsets[3] + 3;
            }
            if (!(sgeom = oddl_new_structure()) || !oddl_structure_add_child(stack[n - 1].parent, sgeom)) {
                free(sgeom);
                ok = 0;
            } else if (!structure_name(sgeom, "geometry", ++numGeoms) || !structure_identifier(sgeom, "GeometryObject")) {
                ok = 0;
            } else if (!(smesh = oddl_new_structure()) || !oddl_structure_add_child(sgeom, smesh)) {
                free(smesh);
                ok = 0;
            } else if (!structure_identifier(smesh, "Mesh") || !oddl_structure_add_string_property(smesh, "primitive", "triangles")) {
                ok = 0;
            } else for (i = 0; ok && i < 5; i++) {
                if (!offsets[i] && i) continue;
                if (!(sarray = oddl_new_structure()) || !oddl_structure_add_child(smesh, sarray)) {
                    free(sarray);
                    ok = 0;
                } else if (!structure_identifier(sarray, "VertexArray") || !oddl_structure_add_string_property(sarray, "attrib", attribs[i])) {
                    ok = 0;
                } else if (!(sdata = oddl_new_data_structure(TYPE_FLOAT32, mesh->numVertices, k = (3 - (i == 2)))) || !oddl_structure_add_child(sarray, sdata)) {
                    free(sdata);
                    ok = 0;
                } else {
                    float *src = mesh->vertices + offsets[i], *dest = sdata->dataList;
                    for (j = 0; j < mesh->numVertices; j++) {
                        memcpy(dest, src, k * sizeof(float));
                        dest += k;
                        src += stride;
                    }
                }
            }
            if (ok && mesh->indices) {
                if (!(sarray = oddl_new_structure()) || !oddl_structure_add_child(smesh, sarray)) {
                    free(sarray);
                    ok = 0;
                } else if (!structure_identifier(sarray, "IndexArray")) {
                    ok = 0;
                } else if (!(sdata = oddl_new_data_structure(TYPE_UINT32, mesh->numIndices / 3, 3)) || !oddl_structure_add_child(sarray, sdata)) {
                    free(sdata);
                    ok = 0;
                } else {
                    unsigned int* src = mesh->indices;
                    uint32_t* dest = sdata->dataList;
                    for (j = 0; j < mesh->numIndices; j++) {
                        *dest++ = *src++;
                    }
                }
            }
            ok = ok && add_ref(&refs, &numRefs, numGeoms, is.vertexArray ? ((void*)dptr.vertexArray) : ((void*)dptr.mesh));
            n--;
            if (is.vertexArray) {
                glUnmapBuffer(GL_ARRAY_BUFFER);
                if (va.numIndices) glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
                viewer_make_current(NULL);
                pthread_mutex_unlock(&shell->swapMutex);
            }
        } else if ((dptr.material = variable_to_material_data(&tmp))) {
            struct ODDLStructure *smat, *sparam, *sdata;
            const char *vAttribs[5], *shader;
            const struct Variable* vars = (const struct Variable*)(dptr.material + 1);
            unsigned int i, nVars;
            int needTextures = 0;
            switch (dptr.material->type) {
                case SOLID: shader = "solid"; nVars = 1; vAttribs[0] = "color"; break;
                case PHONG: shader = "phong"; nVars = 5; vAttribs[0] = "ambient"; vAttribs[1] = "diffuse"; vAttribs[2] = "specular"; vAttribs[3] = "specular_power"; vAttribs[4] = "normal"; break;
                case PBR: shader = "pbr"; nVars = 4; vAttribs[0] = "albedo"; vAttribs[1] = "metalness"; vAttribs[2] = "roughness"; vAttribs[3] = "normal"; break;
                default: ok = 0;
            }
            for (i = 0; ok && i < nVars; i++) {
                struct Variable tmp;
                int ptr = variable_move_dereference(vars + i, &tmp);
                if (variable_is_texture(&tmp)) {
                    if (!get_ref(refs, numRefs, variable_to_texture(&tmp))) {
                        ok = push(&stack, &n, vars + i, 1, NULL);
                        needTextures = 1;
                    }
                } else if (tmp.type != INT && tmp.type != FLOAT && tmp.type != VEC3 && tmp.type != VOID) {
                    ok = 0;
                }
                if (ptr) variable_free(&tmp);
            }
            if (needTextures || !ok) goto next;
            if (!(smat = oddl_new_structure()) || !oddl_structure_add_child(stack[n - 1].parent, smat)) {
                free(smat);
                ok = 0;
            } else if (!structure_name(smat, "material", ++numMats) || !structure_identifier(smat, "Material") || !oddl_structure_add_string_property(smat, "shader", shader)) {
                ok = 0;
            } else for (i = 0; ok && i < nVars; i++) {
                struct Variable tmp;
                int ptr = variable_move_dereference(vars + i, &tmp);
                if (tmp.type == VOID) {
                } else if (!(sparam = oddl_new_structure()) || !oddl_structure_add_child(smat, sparam)) {
                    free(sparam);
                    ok = 0;
                } else if (!oddl_structure_add_string_property(sparam, "attrib", vAttribs[i])) {
                    ok = 0;
                } else if (variable_is_texture(&tmp)) {
                    char* name;
                    if (!(sdata = oddl_new_data_structure(TYPE_STRING, 1, 1)) || !oddl_structure_add_child(sparam, sdata)) {
                        free(sdata);
                        ok = 0;
                    } else if (!structure_identifier(sparam, "Texture") || !(ref = get_ref(refs, numRefs, variable_to_texture(&tmp))) || !(name = malloc(64))) {
                        ok = 0;
                    } else {
                        sprintf(name, "texture%u.png", ref->n);
                        *(char**)(sdata->dataList) = name;
                    }
                } else if (tmp.type == VEC3) {
                    if (!(sdata = oddl_new_data_structure(TYPE_FLOAT32, 1, 3)) || !oddl_structure_add_child(sparam, sdata)) {
                        free(sdata);
                        ok = 0;
                    } else if (!structure_identifier(sparam, "Color")) {
                        ok = 0;
                    } else {
                        memcpy(sdata->dataList, tmp.data.dvec3, sizeof(Vec3));
                    }
                } else if (tmp.type == INT || tmp.type == FLOAT) {
                    if (!(sdata = oddl_new_data_structure(TYPE_FLOAT32, 1, 1)) || !oddl_structure_add_child(sparam, sdata)) {
                        free(sdata);
                        ok = 0;
                    } else if (!structure_identifier(sparam, "Param")) {
                        ok = 0;
                    } else {
                        *(float*)(sdata->dataList) = (tmp.type == FLOAT) ? tmp.data.dfloat : ((float)tmp.data.dint);
                    }
                } else {
                    ok = 0;
                }
                if (ptr) variable_free(&tmp);
            }
            ok = ok && add_ref(&refs, &numRefs, numMats, dptr.material);
            --n;
        } else if ((dptr.texture = variable_to_texture(&tmp))) {
            char* path;
            size_t s = strlen(respath);
            if (get_ref(refs, numRefs, dptr.texture)) {
            } else if (s >= ((size_t)-64) || !add_ref(&refs, &numRefs, ++numTextures, dptr.texture) || !(path = malloc(s + 64))) {
                ok = 0;
            } else {
                memcpy(path, respath, s);
                sprintf(path + s, "/texture%u.png", numTextures);
                ok = ok && png_write(path, dptr.texture->a, dptr.texture->w, dptr.texture->h, dptr.texture->c, 1, (const unsigned char*)(dptr.texture + 1));
                free(path);
            }
            --n;
        } else {
            ok = 0;
        }
        next:
        if (vptr) variable_free(&tmp);
    }
    free(stack);
    free(refs);
    return ok;
}
#endif

static int export_ogex(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
#ifndef TDMR_OPENGEX
    fprintf(stderr, "Error: opengex support was not compiled\n");
    return 0;
#else
    struct ODDLDoc doc;
    struct DemoShell* shell = data;
    FILE* f;
    char *path, *ptr;
    int ok, hasOddl;
    if (!update_scene(shell)) {
        fprintf(stderr, "Error: failed to update nodes\n");
        return 0;
    }
    if (nArgs < 2 || (!(path = variable_to_string(args + --nArgs)))) {
        fprintf(stderr, "Error: export_ogex: invalid arguments\n");
        return 0;
    }
    if (!*path || !(f = fopen(path, "w"))) {
        fprintf(stderr, "Error: failed to open '%s'\n", path);
        free(path);
        return 0;
    }
    if ((ptr = strrchr(path, '/'))) {
        *ptr = 0;
    } else {
        path[0] = '.';
        path[1] = 0;
    }
    if (!(ok = (hasOddl = oddl_init(&doc)) && ogex_set_metrics(&doc))) {
        fprintf(stderr, "Error: failed to initialize ODDL document\n");
    } else if (!(ok = variables_to_ogex(shell, args, nArgs, &doc, path))) {
        fprintf(stderr, "Error: failed to convert argument to ODDL\n");
    }
    free(path);
    if (ok && !(ok = oddl_write(&doc, f))) {
        fprintf(stderr, "Error: failed to write ODDL\n");
    }
    fclose(f);
    if (hasOddl) oddl_free(&doc);
    ret->type = VOID;
    return ok;
#endif
}

int shell_make_opengex_funcs(struct DemoShell* shell) {
    return builtin_function_with_data(shell, "import_ogex", import_ogex, shell)
        && builtin_function_with_data(shell, "export_ogex", export_ogex, shell);
}
